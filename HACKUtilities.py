from __future__ import division
import inspect
import warnings
import collections
import numpy as np               # Import Python's numeric library as an easier-to-use abbreviation, "np"
import pylab as plt              # Import Python's plotting library as the abbreviation "plt"
import pandas as pd
from time import time            # Import Python's "timing" libraries.
from copy import deepcopy
import scipy.stats as stats      # Import Scientific Python's statistics library
from scipy.integrate import quad   # Import quad integration
from scipy.optimize import brentq, fmin, fmin_powell  # Import the brentq root-finder
from matplotlib.backends.backend_pdf import PdfPages
from scipy.interpolate import InterpolatedUnivariateSpline, interp1d
# TODO: Need to look into how the interpolations extend beyond the specified
# region. We likely want linear interpolation to the right, but constant value
# to the left (i.e. no "negative consuption").

# First, a "monkeypatch" to warnings, to print pretty-looking warnings. The
# default behavior of the "warnings" module is to print some extra, silly-
# looking things when the user calls a warning. The official "fix" for this is
# apparently to "monkeypatch" the warnings module. See the discussion here, and
# the included links:
# http://stackoverflow.com/questions/2187269/python-print-only-the-message-on-warnings
# I implement this fix directly below, for all simulation and solution
# utilities.
def _warning(
    message,
    category = UserWarning,
    filename = '',
    lineno = -1):
    print(message)
warnings.showwarning = _warning


# ==============================================================================
# ============== Set up income process functions  ==============================
# ==============================================================================


def calculate_lognormal_discrete_approx(N, mu, sigma):
    '''
    Calculate a discrete approximation to the lognormal distribution.

    Note: SciPy's distributions handled in a general way which ends up having
    strange and perhaps unintended consequences for parameterizing the
    lognomal distribution. See the top answer at the stackoverflow question
    here for more information:
    ["Log Normal Random Variables with Scipy" asked by FooBar on 24Feb 2015](http://stackoverflow.com/questions/28700694/log-normal-random-variables-with-scipy)

    Parameters
    ----------
    N: float
        Size of discrete space vector to be returned.
    mu: float
        mu associated with underlying normal probability distribution.
    sigma: float
        standard deviationassociated with underlying normal probability distribution.

    # TODO: confirm that descriptions are correct.

    Returns
    ----------
    X: np.ndarray
        Discrete points for discrete probability mass function.
    pmf: np.ndarray
        Probability associated with each point in X.

    '''
    # Note: "return" convention will be: values always come first, then probs.
    distrib = stats.lognorm(sigma, 0, np.exp(mu))  # rv = generic_distrib(<shape(s)>, loc=0, scale=1)

    # ------ Set up discrete approx ------
    pdf = distrib.pdf
    invcdf = distrib.ppf

    probs_cutoffs = np.arange(N+1.0)/N      # Includes 0 and 1
    state_cutoffs = invcdf(probs_cutoffs)   # State cutoff values, each bin

    # Set pmf:
    pmf = np.repeat(1.0/N, N)

    # Find the E[X|bin] values:
    F = lambda x: x*pdf(x)
    Ebins = []

    for i, (x0, x1) in enumerate(zip(state_cutoffs[:-1], state_cutoffs[1:])):
        cond_mean1, err1 = quad(F, x0, x1, epsabs=1e-10, epsrel=1e-10, limit=200)
        # Note that the *first* to be fulfilled of epsabs and epsrel stops the
        # intregration - be aware of this when the answer is close to zero.
        # Also, if you never care about one binding (eg you'd like to force
        # scipy to use the other condition to integrate), set that = 0.
        Ebins.append(cond_mean1/pmf[i])

    X = np.array(Ebins)

    return( [X, pmf] )


def calculate_mean_one_lognormal_discrete_approx(N, sigma):
    '''
    Calculate a discrete approximation to a mean-1 lognormal distribution.

    Note: SciPy's distributions handled in a general way which ends up having
    strange and perhaps unintended consequences for parameterizing the
    lognomal distribution. See the top answer at the stackoverflow question
    here for more information:
    ["Log Normal Random Variables with Scipy" asked by FooBar on 24Feb 2015](http://stackoverflow.com/questions/28700694/log-normal-random-variables-with-scipy)

    Parameters
    ----------
    N: float
        Size of discrete space vector to be returned.
    sigma: float
        standard deviationassociated with underlying normal probability distribution.

    # TODO: confirm that descriptions are correct.

    Returns
    ----------
    X: np.ndarray
        Discrete points for discrete probability mass function.
    pmf: np.ndarray
        Probability associated with each point in X.

    '''
    mu = -0.5*(sigma**2)
    return calculate_lognormal_discrete_approx(N=N, mu=mu, sigma=sigma)


def calculate_mean_sacrifice_value(vinv,vhat,xvals,xprobs):
    '''
    Given the inverse of the optimal value function, a value function for an
    approximate policy, and a discrete distribution, return the expected
    sacrifice value.

    Parameters
    ----------
    vinv: univariate real-valued function: vinv:R -> R
        Inverse of the optimal value function.
    vhat: univariate real-valued function: vinv:R -> R
        Value function for approximate policy for which we will find the
        expected sacrifice value.
    xvals: np.ndarray
        Discrete points for discrete probability mass function.
    xprobs: np.ndarray
        Probability associated with each point in xvals.

    Returns
    ----------
    Eeps: float
        Mean sacrifice value.

    '''
    eps = lambda x: x - vinv(vhat(x))
    return np.dot(eps(xvals), xprobs)
