
from __future__ import division
import inspect
import warnings
import collections
import numpy as np               # Import Python's numeric library as an easier-to-use abbreviation, "np"
import pylab as plt              # Import Python's plotting library as the abbreviation "plt"
import pandas as pd
from time import time            # Import Python's "timing" libraries.
from copy import deepcopy
import scipy.stats as stats      # Import Scientific Python's statistics library
from scipy.integrate import quad   # Import quad integration
from scipy.optimize import brentq, fmin, fmin_powell  # Import the brentq root-finder
from matplotlib.backends.backend_pdf import PdfPages
from scipy.interpolate import InterpolatedUnivariateSpline, interp1d
# TODO: Need to look into how the interpolations extend beyond the specified
# region. We likely want linear interpolation to the right, but constant value
# to the left (i.e. no "negative consuption").

# First, a "monkeypatch" to warnings, to print pretty-looking warnings. The
# default behavior of the "warnings" module is to print some extra, silly-
# looking things when the user calls a warning. The official "fix" for this is
# apparently to "monkeypatch" the warnings module. See the discussion here, and
# the included links:
# http://stackoverflow.com/questions/2187269/python-print-only-the-message-on-warnings
# I implement this fix directly below, for all simulation and solution
# utilities.
def _warning(
    message,
    category = UserWarning,
    filename = '',
    lineno = -1):
    print(message)
warnings.showwarning = _warning


# ==============================================================================
# ============== Set up income process functions  ==============================
# ==============================================================================

def calculate_mean_one_lognormal_discrete_approx_with_cutoffs(N, sigma):
    # Note: "return" convention will be: values always come first, then probs.

    mu = -0.5*(sigma**2)
    distrib = stats.lognorm(sigma, 0, np.exp(mu))

    # ------ Set up discrete approx ------
    pdf = distrib.pdf
    invcdf = distrib.ppf

    probs_cutoffs = np.arange(N+1.0)/N      # Includes 0 and 1
    state_cutoffs = invcdf(probs_cutoffs)   # State cutoff values, each bin

    # Set pmf:
    pmf = np.repeat(1.0/N, N)

    # Find the E[X|bin] values:
    F = lambda x: x*pdf(x)
    Ebins = []

    for i, (x0, x1) in enumerate(zip(state_cutoffs[:-1], state_cutoffs[1:])):
        cond_mean1, err1 = quad(F, x0, x1, epsabs=1e-10, epsrel=1e-10, limit=200)
        # Note that the *first* to be fulfilled of epsabs and epsrel stops the
        # intregration - be aware of this when the answer is close to zero.
        # Also, if you never care about one binding (eg you'd like to force
        # scipy to use the other condition to integrate), set that = 0.
        Ebins.append(cond_mean1/pmf[i])

    X = np.array(Ebins)

    return( [X, pmf, state_cutoffs] )



def calculate_mean_one_lognormal_discrete_approx(N, sigma):
    # Note: "return" convention will be: values always come first, then probs.

    X, pmf, state_cutoffs = calculate_mean_one_lognormal_discrete_approx_with_cutoffs(N, sigma)

    return( [X, pmf] )



def calculate_mean_one_lognormal_plus_0_event(N, sigma, p0):
    X, pmf = calculate_mean_one_lognormal_discrete_approx(N, sigma)

    # Readjust the values:
    X = np.append(0.0, X/(1.0 - p0))

    # Update pmf2:
    pmf = np.append(p0, pmf * (1.0 - p0))

    return( [X, pmf] )

def create_shocks_matrix_1D(shocks, N, T, seed=None):
    '''
    Given a vector of equiprobable shocks 'shocks', a number of agents 'N', and
    a time horizon 'T', evenly distribute the shocks across all agents for first
    period, then fill out the rest of the required periods until the time horizon
    via permutations of the first period shocks vector. Print a warning if the
    number of agents does not evenly divide into the number of shock points.

    This is generalizable to M dimentions; such a generalization would simply
    need to create all sets of combinations of M shocks such that each point is
    again equiprobable, and then produce a matrix of results by permuting the
    sets of shocks for each point in time.
    '''

    # Create a RNG with the seed passed in:
    RNG = np.random.RandomState(seed)

    # Create the initial vector of equiprobable shocks:
    z0 = []
    Nshocks = len(shocks)
    number_of_values_per_bin = np.floor(N/Nshocks)
    unplaced_values = N % Nshocks
    if unplaced_values != 0:
        current_func_name = inspect.currentframe().f_code.co_name

        warnings.warn("In "+__name__ + "."+ current_func_name +", the number of shocks len(shocks) = ", str(Nshocks) + " is not an even divisior of the number of agents N = "+ str(N) +". This means that there is not exactly an equiprobably distribution of shocks throughout the population. Observe that N % len(shocks) = " + str(unplaced_values) +".")

    for i in range(Nshocks):
        # First create list of values
        z0 += [shocks[i]]*number_of_values_per_bin

        # Then add a new state to this list if we haven't run out of unplaced_values
        if unplaced_values > 0:
            m0 += [shocks[i]]
            unplaced_values -= 1 # Decrement

    z0_vector = np.array(z0)

    # Now create the matrix of permutations of z0
    z_matrix = [z0_vector]
    for t in range(T-1):
        z_matrix.append(RNG.permutation(z0_vector))

    z_matrix = np.array(z_matrix)

    return z_matrix



def create_flat_state_space_from_2_indep_discrete_probs(X1_pmf1, X2_pmf2):
    '''
    Given two lists whose elements represent two independent, discrete
    probability spaces (points in the space, and a pmf across these points),
    construct a joint pmf over all combinations of these independent points.)

    We flatten because some functions handle 1D arrays better than
    multidimentional arrays. This is particularly true for some lambda-functions
    and interpolation functions applied over arrays, which we employ for
    forming conditional expectations over values tomorrow.
    '''

    X1 = X1_pmf1[0]
    pmf1 = X1_pmf1[1]

    X2 = X2_pmf2[0]
    pmf2 = X2_pmf2[1]

    # Update total state-space:
    nrow = len(X1)
    ncol = len(X2)

    # We don't use the np.meshgrid commands, because they do not
    # easily support non-symmetric grids.
    X1mesh = np.tile(np.transpose([X1]), (1, ncol))
    X2mesh = np.tile(X2, (nrow, 1))

    p1mesh = np.tile(np.transpose([pmf1]), (1, ncol))
    p2mesh = np.tile(pmf2, (nrow, 1))

    # Construct the appropriate probability for each point:
    pmf = p1mesh*p2mesh

    # Flatten:
    flatX1 = X1mesh.ravel()  # We flatten because some functions handle 1D
    flatX2 = X2mesh.ravel()  # arrays better than multidimentional arrays.
    flatpmf = pmf.ravel()    # Particularly nested lambda-functions. (Which may
                             # not be used in the "flat"version of the code.)

    # Double-check:
    #pmfZ = np.zeros_like(X1mesh)
    # Loop to fill in probs:
    #for i, p1 in enumerate(pmf1):
    #    for j, p2 in enumerate(pmf2):
    #        pmfZ[i,j] = p1*p2
    #print "pmf:",pmf
    #print "pmfZ:", pmfZ
    #print "sum(abs(diff)):", np.sum(np.abs(pmf-pmfZ))

    return( [flatX1, flatX2, flatpmf] )

# ==============================================================================
# ============== Set up post-decision state space function  ====================
# ==============================================================================

def setup_grids_expMult(ming, maxg, ng, timestonest=20):
    """
    Set up a Grid.

      Inputs:
          ming - minimum value of the grid
          maxg - maximum value of the grid
          ng   - the number of grid-points
          timestonest - the number of periods
      Output:
          points - a grid for search
    """

    i=1
    gMaxNested = maxg;
    while i <= timestonest:
        gMaxNested = np.log(gMaxNested+1);
        i += 1

    index = gMaxNested/float(ng)

    point = gMaxNested
    points = np.empty(ng)
    for j in range(1, ng+1):
            points[ng-j] = np.exp(point) - 1
            point = point-index
            for i in range(2,timestonest+1):
                points[ng-j] = np.exp(points[ng-j]) - 1

    return(points)


# ==============================================================================
# ============== Set up main solution functions  ===============================
# ==============================================================================

def utility(c, gam):
    return( c**(1.0 - gam) / (1.0 - gam) )

def utilityP(c, gam):
    return( c**-gam )

def utilityPP(c, gam):
    return( -gam*c**(-gam-1.0) )
