'''
learning_library.py

This file contains the library of functions necessary to execute a variety of
solutions to dynamic optimization problems under uncertainty. Note that,
although the title may be taken to indicate that the library contains only
learning-based solutions, the library in fact contains both true optimal dynamic
programming solutions as well as learning solutions.

The current library solves the simple 1-state, 1-shock version of these dynamic
optimization problems. Future libraries will contain extended versions which
take multiple states and / or multiple shocks.

At the moment the functions included still largely need formal testing. They
have been informally tested in the file

    comparing_all_dynamic_programming_approaches__from_all_midpoints_social_learning.py

but a formal testing framework is still in progress.

Create a checklist below and check off each function as it is tested -- often,
at beast, against other different iterations and approaches to solving the same
problem (since most problems do not have a closed-form solution).

By Nathan M. Palmer
On 24 Feb 2015
'''
from __future__ import division
import warnings
import numpy as np
import pylab as plt
from scipy.optimize import fmin, fmin_powell, brute, fminbound
from scipy.interpolate import InterpolatedUnivariateSpline
# ------------------------------------------------------------------------------
#  Import helper functions
# ------------------------------------------------------------------------------
from optimization_library import sacrifice_value, find_value_function
from simulation_library import which_bin, find_conditional_pmf, find_conditional_pmf_from_cons, find_empirical_equiprobable_bins_midpoints, generate_simulated_choice_data_continuous, generate_simulated_shocks_discrete
from HACKUtilities import calculate_mean_sacrifice_value





# ------------------------------------------------------------------------------
# Define learning solutions


def first_visit_value_estimate(u, beta, m_grid, sim_m, sim_c, y_shocks):
    '''
    Create an estimate of a value function using first-pass Monte Carlo
    estimation.

    Parameters
    ----------
    * u:        1D utility function; u:R -> R
    * beta:     discount factor; real (double)
    * m_grid:   state-space grid on which to interpolate value function.
                m_grid must be sorted; ndarray(double)
    * sim_m:    TxN matrix of simulated states following a policy function;
                ndarray(double, double)
    * sim_c:    TxN matrix of policy choices, given sim_m states;
                ndarray(double, double)
    * y_shocks: TxN matrix of simulated shocks, joint with policy rule and law
                of motion these determine the policy function.
    Returns
    -------
    * v:        Value function interpolated over m_grid;
                scipy.interpolate.InterpolatedUnivariateSpline with k=1.
    '''
    # First create all "rewards:"
    sim_u = u(sim_c)

    # Record all first-value bin points:
    first_m = sim_m[0,:]

    # Apply the beta_discounting to each column:
    beta_t = beta ** np.arange(sim_u.shape[0])  # Want number of rows.

    # Copy the beta_t array column-wise, such that it matches the shape of sim_u
    sim_beta = np.tile(beta_t, (sim_u.shape[1], 1)).T

    # Element-wise multiply and then sum:
    discounted_u = sim_u * sim_beta

    # Apply sum to matrix:
    v_estimate = np.apply_along_axis(np.sum, 0, discounted_u)

    # Now organize the m-values such that we can create a function from them.
    #   * "How do we handle collisions?"
    #       * Collisions are actually what we want: we simply averge the N "collision" values.
    #   * Note: the scipy.spatial.cKDTree algorithm warns that
    nearest_m_vals = nearest_sorted_neighbors(m_grid, first_m) # NOTE that m_grid must be sorted.
    v_vals = {} # Will fill this list with v-averages

    for i,j,m in zip(nearest_m_vals, range(len(first_m)), first_m):
        # i will index the nearest value of array *m_grid* which is closest to
        # the corresponding value of *first_m*
        # j will enumerate the values in first_m -- which of course correspond
        # to the values of v_estimate, as well.
        near_m = m_grid[i]
        if not v_vals.has_key(near_m):
            v_vals[near_m] = []

        v_vals[near_m].append(v_estimate[j])

    # At end of the loop we'll have value estimates in lists, linked to points
    # on the m_grid. Now we need to make a second pass, take averages of each
    # point, sort the results, and create the value function. Note that we
    # *probably* could take the average with a "streaming estimate" in the
    # above loop, and skip this loop -- but it would become much less clear to
    # read.
    m_vals = []
    avg_v_vals = []
    for m, w_vals in v_vals.iteritems():
        m_vals.append(m)
        avg_v_vals.append(np.mean(w_vals))

    m_vals = np.array(m_vals)
    avg_v_vals = np.array(avg_v_vals)

    # At end of the loop, get values sorted by m-order and create the function:
    sorted_i = np.argsort(m_vals)
    v = InterpolatedUnivariateSpline(m_vals[sorted_i], avg_v_vals[sorted_i], k=1)

    return v


def single_stream_value_pre_estimate(cons, u, beta, R, x0, y_shock_stream, Nbins):
    # Set up:
    T = len(y_shock_stream)

    c_hist = np.zeros(T) + np.nan
    x_hist = np.zeros(T) + np.nan

    x_hist[0] = x0
    c_hist[0] = cons(x0)

    x_hist[0] = x0
    c_hist[0] = cons(x0)

    betas = beta ** np.arange(T)

    # Run the consumption rule:
    #t = 1
    #y = y_shock_stream[t]    # NOTE that we skip the first income shock! Because we presume that it has been assumed into x0!
    for t in xrange(1,T):    # y in y_shock_stream[1:]:
        x_hist[t] = R*(x_hist[t-1] - c_hist[t-1]) + y_shock_stream[t]
        c_hist[t] = cons(x_hist[t])

    # Now determine bins:
    mids, bins, weights = find_empirical_equiprobable_bins_midpoints(N=Nbins, data=x_hist)

    return mids, bins, weights



def single_stream_value_estimate_old(cons, u, beta, R, x0, bins, midpoints, y_shock_stream, relative_value=False, use_x0_vs_midpoint_i=False):
    """
    Estimate the value function for a provided policy (consumption) function
    using a single stream of shocks.
    """

    # Set up:
    T = len(y_shock_stream)
    Ngrid = len(midpoints)

    c_hist = np.zeros(T) + np.nan
    x_hist = np.zeros(T) + np.nan
    x_vec = np.zeros((T, Ngrid)) + np.nan
    c_vec = np.zeros((T, Ngrid)) + np.nan

    # Set initial conditions:
    x_vec[0,] = midpoints
    if use_x0_vs_midpoint_i:
        i = which_bin(x0, bins)  # These two lines "specalize" this for a single agent.
        x_vec[0,i] = x0
    c_vec[0,] = cons(x_vec[0,])

    # History are the actual experienced values following the consumption rules
    # for these shocks. The "vec" values are use to get the "mental picture" of
    # the value function.
    x_hist[0] = x0
    c_hist[0] = cons(x0)

    betas = beta ** np.arange(T)

    # Run the consumption rule:
    #t = 1
    #y = y_shock_stream[t]    # NOTE that we skip the first income shock! Because we presume that it has been assumed into x0!
    for t in xrange(1,T):    # y in y_shock_stream[1:]:
        x_vec[t,] = R*(x_vec[t-1,] - c_vec[t-1,]) + y_shock_stream[t]
        c_vec[t,] = cons(x_vec[t,])

        x_hist[t] = R*(x_hist[t-1] - c_hist[t-1]) + y_shock_stream[t]
        c_hist[t] = cons(x_hist[t])

    # Now at end of time, can find the value for each bin:
    betas_vec = np.tile(betas, (Ngrid, 1) ).T
    u_vec = u(c_vec)

    if relative_value:
        # Use the relative-value estimator.
        # Need to tile out the y_shock_stream to accomodate the shape of
        # the c_vec/u_vec:
        spendthrift_y = np.tile(y_shock_stream, (Ngrid,1)).T
        spendthrift_y[0,:] = x_vec[0,:]
        #print "u_vec.shape:", u_vec.shape
        #print "spendthrift_y.shape:", spendthrift_y.shape
        u_vec_relative = u_vec - u(spendthrift_y)
        beta_u = betas_vec * u_vec_relative
    else:
        # use the regular estimator:
        beta_u = betas_vec * u_vec

    vals = np.apply_along_axis(np.sum, axis=0, arr=beta_u)

    return vals, midpoints, c_hist, x_hist



def single_stream_first_visit_full_value_estimate(cons, u, beta, Rvec, bins, mids, y_shocks):
    """
    Estimate the value function for a provided policy (consumption) function
    for particular bins, using particular midpoints, using a single stream of
    shocks.


    Parameters
    ----------
    cons: real-valued function  cons:R -> R
        Consumption function used over this period.
    u: real-valued function  u:R -> R
        Utility function.
    Rvec: list of floats (T x 1)
        Return factors over this time range.
    bins: np.ndarray, (1 x (Ngrid-1))
        Cutoff values for each bin. Does *not* include endpoints.
    mids: np.ndarray, (1 x Ngrid)
        Midpoint values for each bin.
    y_shocks: np.ndarray, (T x 1)
        Income shocks over this period.

    Returns
    ----------
    vals: np.ndarray, (1 x Ngrid)
        Value function estimated by first-value Monte Carlo.


    N. Palmer
    """

    # Set up:
    T = len(y_shocks)
    Ngrid = len(mids)

    # Find the "seculative" lives given shocks:
    x_vec, c_vec = speculative_x_c(cons=cons, Rvec=Rvec, bins=bins, mids=mids,
                                   y_shocks=y_shocks)

    # Now at end of time, can find the value for each bin:
    betas = beta ** np.arange(T)
    betas_vec = np.tile(betas, (Ngrid, 1) ).T
    u_vec = u(c_vec)

    # use the regular estimator:
    beta_u = betas_vec * u_vec

    vals = np.apply_along_axis(np.sum, axis=0, arr=beta_u)

    return vals

def speculative_x_c(cons, Rvec, bins, mids, y_shocks):

    # Set up:
    T = len(y_shocks)
    Ngrid = len(mids)

    x_vec = np.zeros((T, Ngrid)) + np.nan
    c_vec = np.zeros((T, Ngrid)) + np.nan

    # Set initial conditions:
    x_vec[0,] = mids
    c_vec[0,] = cons(x_vec[0,])

    # Run the consumption rule:
    # NOTE that we skip the first income shock, because we presume that it has
    # been subsumed into x0, which comes from bin mids.
    for t in xrange(1,T):    # y in y_shocks[1:]:
        x_vec[t,] = Rvec[t]*(x_vec[t-1,] - c_vec[t-1,]) + y_shocks[t]
        c_vec[t,] = cons(x_vec[t,])

    return x_vec, c_vec


def single_stream_value_estimate(cons, u, beta, R, x0, bins, midpoints, y_shock_stream, relative_value=False, use_x0_vs_midpoint_i=False):
    """
    Estimate the value function for a provided policy (consumption) function
    using a single stream of shocks.
    """

    # Set up:
    T = len(y_shock_stream)
    Ngrid = len(midpoints)

    c_hist = np.zeros(T) + np.nan
    x_hist = np.zeros(T) + np.nan
    x_vec = np.zeros((T, Ngrid)) + np.nan
    c_vec = np.zeros((T, Ngrid)) + np.nan

    # Set initial conditions:
    x_vec[0,] = midpoints
    if use_x0_vs_midpoint_i:
        i = which_bin(x0, bins)  # These two lines "specalize" this for a single agent.
        x_vec[0,i] = x0
    c_vec[0,] = cons(x_vec[0,])

    # History are the actual experienced values following the consumption rules
    # for these shocks. The "vec" values are use to get the "mental picture" of
    # the value function.
    x_hist[0] = x0
    c_hist[0] = cons(x0)

    betas = beta ** np.arange(T)

    # Run the consumption rule:
    #t = 1
    #y = y_shock_stream[t]    # NOTE that we skip the first income shock! Because we presume that it has been assumed into x0!
    for t in xrange(1,T):    # y in y_shock_stream[1:]:
        x_vec[t,] = R*(x_vec[t-1,] - c_vec[t-1,]) + y_shock_stream[t]
        c_vec[t,] = cons(x_vec[t,])

        x_hist[t] = R*(x_hist[t-1] - c_hist[t-1]) + y_shock_stream[t]
        c_hist[t] = cons(x_hist[t])

    # Now at end of time, can find the value for each bin:
    betas_vec = np.tile(betas, (Ngrid, 1) ).T
    u_vec = u(c_vec)

    if relative_value:
        # Use the relative-value estimator.
        # Need to tile out the y_shock_stream to accomodate the shape of
        # the c_vec/u_vec:
        spendthrift_y = np.tile(y_shock_stream, (Ngrid,1)).T
        spendthrift_y[0,:] = x_vec[0,:]
        #print "u_vec.shape:", u_vec.shape
        #print "spendthrift_y.shape:", spendthrift_y.shape
        u_vec_relative = u_vec - u(spendthrift_y)
        beta_u = betas_vec * u_vec_relative
    else:
        # use the regular estimator:
        beta_u = betas_vec * u_vec

    vals = np.apply_along_axis(np.sum, axis=0, arr=beta_u)

    return vals, midpoints, c_hist, x_hist




def update_policy_by_regret_ecdf(vals, bins, mids, u, beta, Rvec, x_stream, y_stream, EY=None):
    '''
    Agent does *not* reform all state experiences under new policy. (vs
    reforming all states under "should have been" policy)

    Agent uses ecdf-driven estimation of value tomorrow. (vs only observed
    y-shocks for regret, which would be a version of MC-style)

    Agent may or may not use the entire income stream to find the mean income
    values.
    '''

    D = len(y_stream)

    # Set up container:
    regret_c = np.zeros(D) + np.nan  # TODO: confirm length of regret_c.

    # Define out "regret-finding" function:
    def H(c0, R, x0):
        pmf = find_conditional_pmf(x0=x0, c0=c0, R=R, bins=bins, data=y_stream)
        return -1*(u(c0) + beta * np.dot(vals, pmf)) # Negative for minimization

    # Use state vars as experienced; don't recalculate the states.
    for t in range(D):

        # Find retroactively optimal consumption:
        xopt, fval, ierr, numfunc = fminbound(H, 0.0, x_stream[t], args=(Rvec[t], x_stream[t]), full_output=True, maxfun=2000)

        # Check for numerical solution errors:
        if ierr == 1:
            print "WARNING: did not converge! On step 0."
            xopt = c_stream[t]
        if xopt > x_stream[t]:
            raise Exception, "WARNING: somehow xopt > x_stream[t]!"

        regret_c[t] = xopt

    # Create the state vector corresponding to the c-values. Note that we
    # *don't* want to pass along the full x_stream passed if it is longer than
    # the y-values.
    regret_x = x_stream[0:D]

    # Create expected income value if not provided:
    if EY is None:
        EY = np.mean(y_stream)

    # Find the consumption function.
    mpc, xbar = find_error_minimizing_consumption_ols(c_vals=regret_c,
                                                      x_vals=regret_x, EY=EY)

    # Find the value of regret choices:
    #betas = beta ** np.arange(T)
    #regret_EV = np.dot(u(regret_c), betas)
    #return (mpc, xbar, regret_EV, regret_c, regret_x)

    return (mpc, xbar, regret_c, regret_x)



def find_error_minimizing_consumption_ols(c_vals, x_vals, EY):
    '''
    Find the consumption parameters which minimize the impled consumption
    errors given experience.

    Helper function for "regret_based_policy_update..."

    N Palmer
    '''

    # Now run regression for the informative values we observe:
    n = len(c_vals)
    Y = np.atleast_2d(c_vals).T  # Create column of c-values
    XT = np.vstack( ([1.0]*n, x_vals) )  # Create appropriate X-matrix, in already-inverted form...

    XTX = np.dot(XT,XT.T)   # X'X
    XTY = np.dot(XT,Y)      # X'Y

    Beta = np.linalg.solve(XTX, XTY)
    # Beta = np.dot(np.linalg.inv(XTX),XTY)
    # See: http://www.johndcook.com/blog/2010/01/19/dont-invert-that-matrix/
    # for why I directly solve vs using inversion (b/c numerical stability).

    # Now back out appropriate values for mpc and buffertarget. Recall:
    #   c = E[y] + mpc * (x - xbar)  =   E[y] - mpc*xbar  +  mpc*x
    #                                    \_____ b0 ____/    \_b1_/
    # We will get:
    #   b0 = E[y] - mpc*xbar   =>    xbar = (E[y] - b0) / mpc
    mpc = Beta[1,0] # Second row, first column
    xbar = (EY - Beta[0,0]) / mpc

    return mpc, xbar






def find_error_minimizing_consumption(regret_c, regret_x, EY, current_gamma_xbar, use_ols_minimization):
    '''
    Find the consumption parameters which minimize the impled consumption
    errors given experience.

    Helper function for "regret_based_policy_update..."

    N Palmer
    '''

    # Now run regression for the informative values we observe:
    # Recall function
    # RECALL ALSO: may need to be
    n = len(regret_c)
    if use_ols_minimization:
        Y = np.atleast_2d(regret_c).T  # Create column of c-values
        XT = np.vstack( ([1.0]*n, regret_x) )  # Create appropriate X-matrix, in already-inverted form...

        XTX = np.dot(XT,XT.T)   # X'X
        XTY = np.dot(XT,Y)      # X'Y

        Beta = np.linalg.solve(XTX, XTY)
        # Beta = np.dot(np.linalg.inv(XTX),XTY)
        # See: http://www.johndcook.com/blog/2010/01/19/dont-invert-that-matrix/
        # for why I directly solve vs using inversion (b/c numerical stability).

        # Now back out appropriate values for mpc and buffertarget. Recall:
        #   c = E[y] + mpc * (x - xbar)  =   E[y] - mpc*xbar  +  mpc*x
        #                                    \_____ b0 ____/    \_b1_/
        # We will get:
        #   b0 = E[y] - mpc*xbar   =>    xbar = (E[y] - b0) / mpc
        mpc = Beta[1,0] # Second row, first column
        xbar = (EY - Beta[0,0]) / mpc

    else:
        # Just use direct minimization of objective:
        def obj_fxn(gamma_xbar):
            cvals_new = np.maximum(np.minimum(EY + gamma_xbar[0]*(regret_x-gamma_xbar[1]), regret_x), 0.0)
            SSE = np.sum((regret_c - cvals_new)**2)
            return SSE

        # Minimize:
        OUTPUT = fmin(obj_fxn, current_gamma_xbar, full_output=1)

        xopt = OUTPUT[0]        # Parameter that minimizes function.
        fopt = OUTPUT[1]        # Value of function at minimum: ``fopt = func(xopt)``.
        optiter = OUTPUT[2]     # Number of iterations performed.
        funcalls = OUTPUT[3]    # Number of function calls made.
        warnflag = OUTPUT[4]    # warnflag : int
                                #   1 : Maximum number of function evaluations made.
                                #   2 : Maximum number of iterations reached.
        if warnflag != 0:
            warnings.warn("Minimization of sacrifice value failed! xopt, fopt, optiter, funcalls, warnflag = "+ str(xopt) +", "+ str(fopt) +", "+ str(optiter) +", "+ str(funcalls) +", "+ str(warnflag) +"\n Defaulting to previous mpc, xbar.")
            mpc = current_gamma_xbar[0]
            xbar = current_gamma_xbar[1]
        else:
            mpc = xopt[0]
            xbar = xopt[1]

    # Done.
    return mpc, xbar


def regret_based_policy_update_reform_xstream(vvals, bins, mids, u, beta, R, EY, current_gamma_xbar, x_stream, c_stream, y_stream, x0, sophisticated_regret=False, y_hist=None, full_yhist=False, approx_value=False, use_ols_minimization=True, trim_x=False, trim_pctles=(5,95)):
    '''
    Extremely important note: y_stream[0] is *not* used explicitly in the loop
    below because it is assumed to be incorporated already into x0. Be aware
    of this convention when selecting y_stream and x0!


    sophisticated_regret implies that agent will re-figure all experience state vals
    under new choices.
    '''

    D = len(y_stream)
    #c_regret_optimal = np.zeros(D) + np.nan  # NOTE: may need a D-1 value here.

    # For each choice, back out what *would* have been the optimal choice. Then
    # do simple OLS fit over all informative regret-choices.

    regret_c = []
    #uninformative_c = []

    regret_x = []
    #uninformative_x = []

    #x=x0
    #x1 = R*(x0-c) + y_stream[1]

    # Find "I should have ..." choice:

    #i = which_bin(x1, bins)
    #v1 = vvals[i]
    #H = lambda c: -1*(u(c) + beta * vvals[i])

    # Define out "regret-finding" function:
    if approx_value:
        # "Properly" approximate the value function using implied states
        # states tomorrow:
        if full_yhist:
            # Use full y-history for this agent thus far.
            def H(c0, R, x0, y1):
                pmf = find_conditional_pmf(x0=x0, c0=c0, R=R, bins=bins, data=y_hist)
                return -1*(u(c0) + beta * np.dot(vvals, pmf))
        else:
            # Use only y-history for this "learning stretch:"
            def H(c0, R, x0, y1):
                pmf = find_conditional_pmf(x0=x0, c0=c0, R=R, bins=bins, data=y_stream)
                return -1*(u(c0) + beta * np.dot(vvals, pmf))
    else:
        # Simply use nearest neighbor approximation, ignore information from
        # income experience:
        def H(c0, R, x0, y1):
            x1 = R*(x0-c0) + y1
            i = which_bin(x1, bins)
            return -1*(u(c0) + beta * vvals[i])

    #def H2(c0, R, x0, y1):
    #    x1 = lambda z, R=R, x0=x0, c0=c0:  R*(x0-c0) + z
    #    #i = which_bin(x1, bins)
    #    return -1*(u(c0) + beta * np.dot(vtemp(Ypoints), Yprobs))

    # BE EXTREMELY CAREFUL OF TIMING!

    # Set x0 from input or previous consumption
    # set c from x0
    # find regret-based-c
    # WHERE AT: HERE. Need to think carefully about the way the timing works here!
    ''' Timing:
    x0, ->  back out what "should have done" with greedy choice -> c0
    '''

    # Start the loop:
    # find next-x from previous-c choice
    # find "regret-based" c-choice
    # loop...
    #x = x0

    # Given x0, back out the regretted c0, then update new x, continue...
    x_vec = np.zeros(D) + np.nan
    c_vec = np.zeros(D) + np.nan
    x_vec[0] = x0

    # Find optimal-ish consumption:
    xopt, fval, ierr, numfunc = fminbound(H, 0.0, x_vec[0], args=(R, x_vec[0], y_stream[1]), full_output=True, maxfun=2000)
    if ierr == 1:
        print "WARNING: did not converge! On step 0."
        xopt = c_stream[0]
    c_vec[0] = xopt

    # Check for error:
    if xopt > x_vec[0]:
        raise Exception, "WARNING: somehow xopt > x_vec[0]!"
    regret_c.append(c_vec[0])
    regret_x.append(x_vec[0])

    if sophisticated_regret:
        # Re-calculate all state vars under the new choices.
        for t in range(1,D-1):

            # TODO: confirm that timing is correct for the y-shocks -- both looking
            #       back (for x, c) and looking ahead (for y)

            # Update x for next period:
            x_vec[t] = R*(x_vec[t-1] - c_vec[t-1]) + y_stream[t]

            # Find retroactively optimal consumption:
            xopt, fval, ierr, numfunc = fminbound(H, 0.0, x_vec[t], args=(R, x_vec[t], y_stream[t+1]), full_output=True, maxfun=2000)
            # Find optimal-ish consumption:
            if ierr == 1:
                print "WARNING: did not converge! On step 0."
                xopt = c_stream[t]

            c_vec[t] = xopt

            # Determine if there is an error:
            if xopt > x_vec[t]:
                raise Exception, "WARNING: somehow xopt > x_vec[t]!"
            regret_c.append(c_vec[t])
            regret_x.append(x_vec[t])
        # At end of loop, either return "no change"/None/False or return the new
        # parameters.
    else:
        # Just use state vars as experienced; don't recalculate the states.
        #print "x_stream =", x_stream
        #print "len(x_stream) =", len(x_stream)
        for t in range(1,D-1):
            # Update x for next period:
            try:
                x_vec[t] = x_stream[t]

                # Find retroactively optimal consumption:
                xopt, fval, ierr, numfunc = fminbound(H, 0.0, x_vec[t], args=(R, x_vec[t], y_stream[t+1]), full_output=True, maxfun=2000)

                # Find optimal-ish consumption:
                if ierr == 1:
                    print "WARNING: did not converge! On step 0."
                    xopt = c_stream[t]

                c_vec[t] = xopt

                # Determine if the c-value is informative:
                if xopt > x_vec[t]:
                    raise Exception, "WARNING: somehow xopt > x_vec[t]!"
                regret_c.append(c_vec[t])
                regret_x.append(x_vec[t])
            except Exception,e:
                print "Had this error: ", str(e)
        # Done

    # After loop, set values as arrays:
    regret_x = np.array(regret_x)
    regret_c = np.array(regret_c)

    # Check to see if need to "trim" the values:
    if trim_x:
        xlo, xhi = np.percentile(regret_x, trim_pctles)
        save_index = np.logical_and(regret_x >= xlo, regret_x <= xhi)
        regret_x = regret_x[save_index]
        regret_c = regret_c[save_index]

    # Find the consumption function:
    mpc, xbar = find_error_minimizing_consumption(regret_c=regret_c,
                    regret_x=regret_x, EY=EY,
                    current_gamma_xbar=current_gamma_xbar,
                    use_ols_minimization=use_ols_minimization)

    return (mpc, xbar, regret_c, regret_x)


# Define a function which creates an agent, and
# Define a function which runs that agent's learning process for T periods
# Make sure to record the appropriate values from the agent experience. In fact,
# encapsulate all useful information into a dict and save that dict to the
# "population." Then later pull all value from the 'population.'

def create_agent_run_1_learning_experience(x0, D, TT, auto_mids_bins, auto_mids_bins_full_yhist, EY_equals_mean, shocks, probs, income_generator, u, beta, R, Nbins_for_regret_learning, relative_value, agents_sophisticated_regret, use_full_full_yhist_in_ecdf_vprime, use_ecdf_to_get_vprime, use_ols_minimization, trim_x, trim_pctles, EY=None, gamma_xbar=None, mids=None, bins=None, weights=None):
    '''
    Where do these passed-in values come from?
    x0=_____,D = param.D_autolearn, TT = param.T_autolearn, auto_mids_bins = param.auto_mids_bins, auto_mids_bins_full_yhist = param.auto_mids_bins_full_yhist, EY_equals_mean = param.EY_equals_mean, shocks=param.shocks, probs=param.probs, income_generator=param.income_generator, u=param.u, beta=param.beta, R=param.R, Nbins_for_regret_learning=param.Nbins_for_regret_learning, relative_value=param.relative_value, agents_sophisticated_regret=param.agents_sophisticated_regret, use_full_full_yhist_in_ecdf_vprime=param.use_full_full_yhist_in_ecdf_vprime, use_ecdf_to_get_vprime=param.use_ecdf_to_get_vprime, use_ols_minimization=param.use_ols_minimization, trim_x=param.trim_x, trim_pctles=param.trim_pctles, EY=None, gamma_xbar=None, mids=None, bins=None, weights=None,
    '''


    # Initialize data container
    y_hist = np.array([])
    c_hist = np.array([])
    x_hist = np.array([])

    # Set EY to passed-in value or value from params/probs:
    if EY is None:
        EY  = np.dot(shocks, probs)  # Expected income

    # Init consumption rule:
    # If no initial rule is provided, set up init params to "consume everything"
    # Recall:
    # c(x) =  EY + mpc*(x-xbar) = EY + mpc*x - mpc*xbar
    if gamma_xbar is None:
        mpc = 1.0
        gamma_xbar = (mpc, EY)
    else:
        mpc = gamma_xbar[1]


    # Save consumption parameters and rule:
    consumption_rules = []
    params = []
    all_regret_c = []
    all_regret_x = []

    d = D

    # TODO: MUST write a version of this which is *only* for one or two types of model!
    # MUST ensure that I am not mixing up or incorrectly creating values!

    max_c = -np.inf

    #use_true_value = False
    mids_list = []
    learn_counter = 0
    all_periods = []
    all_EY_to_use = []
    last_t = 0
    while d <= TT:

        # Generate the income for this chunk of time:
        y_stream_temp = income_generator(D)

        # Save the period in which learning occurred:
        all_periods.append(d)

        # Save the income generated:
        y_hist = np.append(y_hist, y_stream_temp)

        # Save the consumption rule and parameters used:
        cons = LinearConsumpRule(gamma=gamma_xbar[0], mbar=gamma_xbar[1], ID=gamma_xbar, C0=EY)
        consumption_rules.append(deepcopy(cons))
        params.append(deepcopy(gamma_xbar))

        # Steps:
        #   0. Find the mids, bins, and weights associated with experience.
        #   1. Find value function associated with experience
        #   2. Find regret c and x values, and find new consumption rules consistent with these
        #   3. Record values and loop

        # Does the agent use bins only from own experience, or handed from on
        # high?
        no_bins_provided = mids is None or bins is None
        if no_bins_provided or auto_mids_bins:

            # Should we use the entire income experience to find our bins and
            # mids?
            if auto_mids_bins_full_yhist:
                # Use y_hist, all of agent's experience
                mids, bins, weights = single_stream_value_pre_estimate(cons=cons,
                                        u=u, beta=beta,
                                        R=R, x0=x0,
                                        y_shock_stream=y_hist,
                                        Nbins=Nbins_for_regret_learning)
            else:
                # use y_stream_temp, only the experience from this d-length trial
                mids, bins, weights = single_stream_value_pre_estimate(cons=cons,
                                        u=u, beta=beta,
                                        R=R, x0=x0,
                                        y_shock_stream=y_stream_temp,
                                        Nbins=Nbins_for_regret_learning)

        # Record the mids used:
        mids_list.append(deepcopy(mids))


        # Learn values from experience:
        if use_full_yhist_for_value_learn:

            values_to_use, xmids, c_stream_temp, x_stream_temp = single_stream_value_estimate(cons=cons,
                    u=u, beta=beta, R=R, x0=x0,
                    bins=bins, midpoints=mids,
                    y_shock_stream=y_stream_temp,
                    relative_value=relative_value)
        else:
            values_to_use, xmids, c_stream_temp, x_stream_temp = single_stream_value_estimate(cons=cons,
                    u=u, beta=beta, R=R, x0=x0,
                    bins=bins, midpoints=mids,
                    y_shock_stream=y_hist,
                    relative_value=relative_value)

        # TODO: Error-test these lines! DO they do what I want??

        print '======================'
        print "step is d =", d
        print "len(x_stream_temp) =" , len(x_stream_temp)
        print "len(c_stream_temp) =" , len(c_stream_temp)

        print "x_stream_temp =", x_stream_temp
        print "c_stream_temp =", c_stream_temp
        print '---------------------'

        if len(c_stream_temp) == D:
            print "len(c_stream_temp) == D"
            c_hist = np.append(c_hist, c_stream_temp)  # Only want the slice that we really care about.
        elif len(c_stream_temp) > D:
            print "len(c_stream_temp) > D"
            c_hist = np.append(c_hist, c_stream_temp[last_t:(d+1)])  # Only want the slice that we really care about.
        else:
            print "Have a problem here."

        if len(x_stream_temp) == D:
            print "len(x_stream_temp) == D"
            x_hist = np.append(x_hist, x_stream_temp)  # Only want the slice that we really care about.
        elif len(c_stream_temp) > D:
            print "len(x_stream_temp) > D"
            x_hist = np.append(x_hist, x_stream_temp[last_t:(d+1)])  # Only want the slice that we really care about.
        else:
            print "Have a problem here."


        # Determine current EY value
        if EY_equals_mean == 1:
            EY_to_use = np.mean(y_hist)
        elif EY_equals_mean == 2:
            EY_to_use = np.mean(y_stream_temp)
        else:
            EY_to_use = EY  # Use whatever was passed in or set.

        all_EY_to_use.append(EY_to_use)

        # Now find the  at the values we pull out from our experience:
        if use_full_yhist_for_value_learn:

            print "x_stream_temp =",x_stream_temp
            print "len(x_stream_temp) =", len(x_stream_temp)
            print "x_hist =",x_hist
            print "len(x_hist) =", len(x_hist)
            print "d =",d
            print "last_t =", last_t

            mpc, xbar, regret_c, regret_x = regret_based_policy_update_reform_xstream(vvals=values_to_use,
                                                                  bins=bins,
                                                                  mids=mids, u=u,
                                                                  beta=beta,
                                                                  R=R,
                                                                  current_gamma_xbar=[cons.gamma, cons.mbar],
                                                                  EY=EY_to_use,
                                                                  x_stream=x_stream_temp,
                                                                  c_stream=c_stream_temp,
                                                                  y_stream=y_hist,
                                                                  x0=x0,
                                                                  sophisticated_regret=agents_sophisticated_regret,
                                                                  y_hist=y_hist,
                                                                  full_yhist=use_full_full_yhist_in_ecdf_vprime,
                                                                  approx_value=use_ecdf_to_get_vprime,
                                                                  use_ols_minimization=use_ols_minimization,
                                                                  trim_x=trim_x,
                                                                  trim_pctles=trim_pctles)
        else:
            mpc, xbar, regret_c, regret_x = regret_based_policy_update_reform_xstream(vvals=values_to_use,
                                                                  bins=bins,
                                                                  mids=mids, u=u,
                                                                  beta=beta,
                                                                  R=R,
                                                                  current_gamma_xbar=[cons.gamma, cons.mbar],
                                                                  EY=EY_to_use,
                                                                  x_stream=x_stream_temp,
                                                                  c_stream=c_stream_temp,
                                                                  y_stream=y_stream_temp,
                                                                  x0=x0,
                                                                  sophisticated_regret=agents_sophisticated_regret,
                                                                  y_hist=y_hist,
                                                                  full_yhist=use_full_full_yhist_in_ecdf_vprime,
                                                                  approx_value=use_ecdf_to_get_vprime,
                                                                  use_ols_minimization=use_ols_minimization,
                                                                  trim_x=trim_x,
                                                                  trim_pctles=trim_pctles)

        all_regret_c.append(deepcopy(regret_c))
        all_regret_x.append(deepcopy(regret_x))
        #EY=np.mean(y_stream_temp)
        #params.append((mpc, xbar))
        revert_flag = False
        if mpc <=0:
            if len(params) == 1:
                print "On step d=",d,"mpc=",mpc,"-- defaulting to previous mpc and xbar."
            elif len(params) == 2:
                print "On step d=",d,"mpc=",mpc,"-- defaulting to convex combination of last 2 previous mpc and xbar values."
            else:
                print "On step d=",d,"mpc=",mpc,"-- defaulting to convex combination of last 3 previous mpc and xbar values."
            revert_flag = True
            #mpc = params[-1][0]

        if xbar <=0:
            if len(params) == 1:
                print "On step d=",d,"xbar=",xbar,"-- defaulting to previous mpc and xbar."
            elif len(params) == 2:
                print "On step d=",d,"xbar=",xbar,"-- defaulting to convex combination of last 2 previous mpc and xbar values."
            else:
                print "On step d=",d,"xbar=",xbar,"-- defaulting to convex combination of last 3 previous mpc and xbar values."
            revert_flag = True
            #xbar = params[-1][1]

        if revert_flag:
            if len(params) == 1:
                mpc = params[-1][0]  # Previous
                xbar = params[-1][1]
            elif len(params) == 2:
                mpc = 0.5*params[-1][0] + 0.5*params[-2][0]  # Convex combo of last two
                xbar = 0.5*params[-1][1] + 0.5*params[-2][1]
            else:
                mpc = 1.0/3*params[-1][0] + 1.0/3*params[-2][0] + (1.0-(1.0/3+ 1.0/3))*params[-3][0]  # Convex combo of last three
                xbar = 1.0/3*params[-1][1] + 1.0/3*params[-2][1] + (1.0-(1.0/3+ 1.0/3))*params[-3][1]

        new_gamma_xbar = (mpc, xbar)

        #learned_functions2.append( LinearConsumpRule(gamma=mpc, mbar=xbar, ID=(mpc, xbar), C0=EY) )
        max_c = np.maximum(max_c, np.max(regret_c))

        new_cons = LinearConsumpRule(gamma=mpc, mbar=xbar, ID=(mpc, xbar), C0=EY_to_use)

        # Now set values for the next pass:
        x0 = x_hist[-1] #x_stream_temp[-1]  # Last value experienced...
        cons = new_cons
        gamma_xbar = new_gamma_xbar
        learn_counter += 1
        last_t = d
        d += D

    # At the end of the loop, need to return a number of values: parameters
    # learned at each interval, consumption and x-values from entire learning
    # experience, and ... anything else that seems to really define the agent:
    return {'y_hist':y_hist,'c_hist':c_hist,'x_hist':x_hist,'all_periods':all_periods,'all_EY':all_EY_to_use,'params':params}




if __name__ == "__main__":

    # Use this space specifically to test out and execute functionality that I'd
    # like to use in the "technical briefs" overview.
    # Meant to give intution without diving into details.

    # First thing to put together:
    # Demonstration of:
    #   0.a true optimal consumption function
    #   0.b one long single stream of income
    #   0.c image of convergence
    #   (a) individual-stream of income *nonconvergence* of individual-level, single-
    # experience learning, and (b)
    #
    #
    # NOTE: will eventually need a discussion of the binning process itself.
    # Initially this will be relgated to the technical appendix, but eventually
    # may need to take a center stage with respect to explaining why the process
    # should be expected to coverge to the truth.

    # Import the parameters needed for this demonstration:
    import setup_parameters_functions_regret_learning_computational_letter as param
    from optimization_library import generalized_policy_iteration
    from basic_code_structure import LinearConsumpRule
    from progress_bar import ProgressBar
    from copy import deepcopy
    from time import time
    import pylab as plt


    # Should we show all plots?
    all_sequential_plots = param.all_sequential_plots



    print '''
    # ------------------------------------------------------------------------ #
    # ------------- Create consumption and wealth distrib. plot     ---------- #
    # ------------- Find optimal consumption function, find cutoffs ---------- #
    # ------------------------------------------------------------------------ #
    '''

    # First find the true optimal consumption function:
    optcons, optval = generalized_policy_iteration(u=param.u, f=param.f,
                                                   beta=param.beta, grid=param.grid,
                                                   tol=param.tol, Ypoints=param.Ypoints,
                                                   Yprobs=param.Yprobs, v=None,
                                                   policy=None, verbose=param.verbose)

    # Simulate consumption rule and find 5%, 95% conf intervals
    sim_m, sim_c, y_shocks, mvals = generate_simulated_choice_data_continuous(cons=optcons,
                                                  income_generator=param.income_generator,
                                                  periods=param.totalT, Nagents=param.Nagents, R=param.R,
                                                  discard_periods=param.discarded_periods, m0=param.m0)

    # Create a flattened version of m-values, for many future uses:
    flat_sim_m = sim_m.ravel()

    # Find a discrete approximation to the the distribution of m-values:
    Nbins_for_sacrifice_approx = min(501, len(flat_sim_m))
    sim_m_mids, sim_m_bins, sim_m_probs = find_empirical_equiprobable_bins_midpoints(N=Nbins_for_sacrifice_approx, data=flat_sim_m)

    # Find the 5th, 95th percentiles of the simulated data:
    sim_mC_5_95_percentiles = np.percentile(flat_sim_m, [5.0, 95.0])

    # Plot true optimal function with 5th, 95th percentiles
    upperbound = np.ceil(np.max(optcons(param.grid)))

    if all_sequential_plots:
        plt.hist(flat_sim_m,normed=True, label="Cash on hand")
        plt.plot(param.grid, optcons(param.grid), 'r--', label="Optimal policy")
        plt.vlines(sim_mC_5_95_percentiles, ymin=0, ymax=1.2*upperbound, linestyles='dotted', label='5th, 95th percentiles')
        plt.ylim(0, upperbound)
        plt.hold(True)
        plt.legend(loc='best', frameon=False)
        plt.title("True Optimal Consumption and\nDistribution of Cash-on-Hand")
        plt.show()


    print '''
    # ------------------------------------------------------------------------ #
    # --------- Plot one long income draw ------------------------------------ #
    # ------------------------------------------------------------------------ #
    '''

    # Now show example of long income draw
    example_single_long_income_T = param.example_single_long_income_T
    single_long_T_str_format = "{:,}".format(example_single_long_income_T)

    # Create single long stream of income:
    y_stream = param.income_generator(example_single_long_income_T)

    # Plot
    if all_sequential_plots:

        plt.plot(y_stream, 'b-', label="Income")
        plt.plot(optcons(y_stream), 'r:', label="Consumption")
        plt.legend(loc='best', frameon=False)
        plt.title("Example: " +single_long_T_str_format+ "-period Income Draw\nwith Optimal Consumption Choices")
        plt.show()


    '''
    # ------------------------------------------------------------------------ #
    # ------- Learn the value function from one long income draw ------------- #
    # ------------------------------------------------------------------------ #

    # Now plot results of learning value function from a single income stream

    # First find the bins and midpoints for the agents who will be learning. Use
    # the simulated data from following the bins which will give us the ergodic
    # distribution of values
    Nbins = param.Nbins_for_regret_learning
    mids, bins, weights = find_empirical_equiprobable_bins_midpoints(N=Nbins, data=flat_sim_m)


    # Create an evenly-spaced periods-to-show from 0 to example_single_long_income_T
    N_show_v_estimates = param.N_show_single_stream_value_estimates
    #stepsize = np.floor(example_single_long_income_T/float(N_show_v_estimates))
    #periods_to_show = np.arange(start=N_show_v_estimates,
    #                            stop=example_single_long_income_T,
    #                            step=stepsize)

    # We need to use exponential / log weighting of the "display values" so we
    # cajn actually see things changing at a reasonable pace.
    disp_min = 1
    disp_max = example_single_long_income_T
    N_show_v_estimates = param.N_show_single_stream_value_estimates
    disp_grid_log = np.linspace(np.log(disp_min), np.log(disp_max), N_show_v_estimates)
    disp_grid = np.floor(np.exp(disp_grid_log))  # Must use floor to impose integers
    disp_grid = np.unique(disp_grid)             # Get rid of duplicates due to "floor"

    #periods_to_show = [1,3,5,7,10,15,20,27,37,50,70, 90, 115, 400, 10000]

    # Alternate way to build up the periods to show: choose a log change step size, and only show that.
    # log_change = 0.7
    # disp_grid_log = np.arange(np.log(disp_min), np.log(disp_max), step=log_change)
    # disp_grid = np.floor(np.exp(disp_grid_log))
    # print "len(disp_grid)",len(disp_grid),"len(np.unique(disp_grid))", len(np.unique(disp_grid))
    # disp_grid = np.unique(disp_grid)

    periods_to_show = disp_grid

    # Putting in:
    # * [ ] progress bar
    # * [ ] text explain the log spacing


    # For each of the period-markers above, estiamte off the single stream of
    # income and plot the resulting value functions. Note that this display is
    # very inefficient, but shouldn't matter for plotting.

    # Set up progress bar quickly:
    # progbar_len=20
    # loop_len = len(periods_to_show)
    # if progbar_len >= loop_len:
    #     progbar_len = loop_len
    #     progbar_mod = 1
    # else:
    #     progbar_mod = np.floor(loop_len / progbar_len)


    # "We can see that, for the very long single draw of an income time series,
    # the values estimated seem to convernge:"
    print "Single long estimation with 1 agent..."
    pbar = ProgressBar(loop_length=len(periods_to_show), progbar_length=param.progbar_len, symbol='=')

    t0 = time()
    for i, tt in enumerate(periods_to_show):
        vCtemp, xmids, c_stream, x_stream = single_stream_value_estimate(cons=optcons, u=param.u, beta=param.beta, R=param.R, x0=param.m0, bins=bins, midpoints=mids, y_shock_stream=y_stream[:tt], relative_value=param.relative_value)
        if all_sequential_plots:
            plt.plot(xmids, vCtemp, 'b-+')
        pbar.update(i)
        #if i%progbar_mod == 0:
        #    pbar.update() #(i)


    pbar.end()
    t1 = time()

    if all_sequential_plots:
        plt.title("Value Estimates: Single "+single_long_T_str_format+"-Length Experience with Opt. Consumption.\nDisplay is Approx. Log-Spaced* t: {" +str(int(periods_to_show[0]))+ ", " +str(int(periods_to_show[1]))+ ", " +str(int(periods_to_show[2]))+ ",...," +str(int(periods_to_show[-2]))+ ", " +str(int(periods_to_show[-1]))+ "}")
        plt.figtext(.09, .02, "*Approximate because t-values are rounded to nearest whole integer.")
        plt.show()

    print "TIME IS: " + str( (t1-t0) ) + " sec."

    # "Unfortunately this is not fully converging. There is, in fact, a proof that
    # finite time will be biased for a single stream draw."

    # Make sure that we get the full income stream:
    vC, xmids, c_stream, x_stream = single_stream_value_estimate(cons=optcons, u=param.u, beta=param.beta, R=param.R, x0=param.m0, bins=bins, midpoints=mids, y_shock_stream=y_stream, relative_value=param.relative_value)

    if all_sequential_plots:
        plt.plot(xmids, vC, 'b-+', label=single_long_T_str_format+"-period estimate")
        plt.plot(xmids, optval(xmids), 'r--+', label="True value function")
        plt.legend(loc='best', frameon=False)
        plt.title("Single-Stream Estimate vs. True Value Function")
        plt.show()


    # ------------------------------------------------------------------------ #
    # ------- Learn the value function from one long income draw ------------- #
    # ------- and many friends.                                  ------------- #
    # ------------------------------------------------------------------------ #


    # However, we *can* see that if we average over enough agents with
    # independent income draws, we *will* find the correct value function:

    # Generate N_trial_agents iid streams, have agents learn, look at average.
    value_functions_list = []
    N_trial_agents = param.N_friends_for_ave_demo
    TT = param.T_periods_for_ave_demo
    # We will sample the m0 values from the flat_sim_m set, using the RNG from
    # the param file.

    # Calculate the "variance-reducing" number of agents, given the number of
    # bins we have selected. "What is the number of agents which most closely
    # evenly evenly divided by number of shock bins?"

    #N_left_over_agents = param.N_friends_for_ave_demo % param.N  # remainder for N_agents / N_bins
    #N_trial_agents = (param.N_friends_for_ave_demo + (param.N-N_left_over_agents))  # We can choose to reduce the number of agents slightly or increase the number of agents slightly to make N_agents evenly divisible by N_bins. This increases.


    print "Estimation with many ("+ "{:,}".format(N_trial_agents) +") agents..."

    pbar = ProgressBar(loop_length=N_trial_agents, progbar_length=param.progbar_len, symbol='$')
    #m0_vector = param.RNG.choice(a=flat_sim_m, size=N_trial_agents, replace=False) # Bootstrap from the sample?
    # Let's grab an equiprobable sample of the starting positions. And then ... generate the "appropriate"
    # set of shocks to minimize simulation variance? "Stacking deck," so be clear about that.

    m0_vector = generate_simulated_shocks_discrete(mids, T=1, N=N_trial_agents, seed=param.example_single_long_income_seed)
    y_shocks_vector = generate_simulated_shocks_discrete(param.shocks, T=TT, N=N_trial_agents, seed=(param.example_single_long_income_seed+1))
    t0 = time()
    for i in range(N_trial_agents):
        #m0_temp = flat_sim_m[-(i+1)]
        m0_temp = m0_vector[0,i]
        #m0_temp = param.m0

        #y_stream_temp = param.income_generator(TT)
        y_stream_temp = y_shocks_vector[:,i]
        v_temp, xmids, c_stream, x_stream = single_stream_value_estimate(cons=optcons,
                    u=param.u, beta=param.beta, R=param.R,
                    x0=m0_temp, bins=bins, midpoints=mids,
                    y_shock_stream=y_stream_temp,
                    relative_value=param.relative_value)
        value_functions_list.append(deepcopy(v_temp))

        # Plot the value functions; only label the first line.
        if all_sequential_plots:
            if i == 0:
                plt.plot(xmids, deepcopy(v_temp), 'b-', label='Individual experience estimates')
            else:
                plt.plot(xmids, deepcopy(v_temp), 'b-')
        del y_stream_temp
        del v_temp
        pbar.update(i)
    pbar.end()
    t1 = time()
    print "Time: " + str((t1-t0)/60.) +" min"

    avg_val = []
    for i,x in enumerate(mids):
        tempv = 0
        for j in range(N_trial_agents):
            tempv += value_functions_list[j][i]
        avg_val.append(tempv / float(N_trial_agents))

    if all_sequential_plots:
        plt.plot(mids, avg_val, 'r-', label="Average of "+str(N_trial_agents) +" experiences")
        plt.plot(mids, optval(mids), 'k-', label="True")
        plt.legend(loc='best', frameon=False)
        plt.title(str(N_trial_agents) + ' Agents for ' + str(TT) + ' Periods.\n '+str(len(mids))+' Aggregated States.\nUsing Optimal Policy')
        plt.show()

    # "Let's zoom in on the average vs the true policy."

    #print "HEEEEEEEEEEEEEEEEEEEEY"
    if all_sequential_plots:
        plt.plot(mids, vC, 'r--+', label="1-agent example learning")
        plt.plot(mids, avg_val, 'r-', label="Average of "+str(N_trial_agents) +" experiences")
        plt.plot(mids, optval(mids), 'k-', label="True")
        plt.legend(loc='best', frameon=False)
        plt.title("Examine more closely:\n"+str(N_trial_agents) + ' Agents for ' + str(TT) + ' Periods.\n'+str(len(mids))+' Aggregated States. Using Optimal Policy')
        plt.show()

    '''

    print '''
    # ------------------------------------------------------------------------ #
    # ------- Now examine learning from agents own experience only ----------- #
    # ------- This is "almost true" individual-level learning      ----------- #
    # ------- (Next step will be deriving bins + mids from exper.) ----------- #
    # ------------------------------------------------------------------------ #
    '''

    # Define the inverse of optimal value function:
    zx_vals = optval.get_coeffs()
    zy_vals = optval.get_knots()
    vopt_invs = InterpolatedUnivariateSpline(x=zx_vals[1:], y=zy_vals[1:], k=1)  # TODO: confirm that this "lose the first" needed


    # Plot last line as well as scatter of points:
    Dlo = 0
    D = param.D_autolearn
    TT = param.T_autolearn

    if TT > y_shocks.shape[0]:
        print "Reset TT from ",TT, "to", y_shocks.shape[0]
        TT = y_shocks.shape[0]

    #y_stream = y_shocks(:TT) # Just grab the first simulated income draw from earlier

    y_hist = np.array([])

    # Linear rule:
    # c(x) =  EY + mpc*(x-xbar) = EY + mpc*x - mpc*xbar
    # Set up initial parameters for "consum everything:"
    mpc = 1.0
    EY  = np.dot(param.shocks, param.probs)  # Expected income
    gamma_xbar = (mpc, EY)

    consumption_rules = []
    params = []
    all_regret_c = []
    all_regret_x = []

    d = D
    x0 = param.m0           # Try the extremes of each...
    eps_vals = []

    auto_mids_bins = param.auto_mids_bins
    auto_mids_bins_full_yhist = param.auto_mids_bins_full_yhist
    use_full_yhist_for_value_learn = param.use_full_yhist_for_value_learn

    max_c = -np.inf

    use_true_value = False
    values_list = []
    mids_list = []
    learn_counter = 0
    plotgrid = param.plotgrid
    all_periods = []
    all_EY_to_use = []
    all_current_gamma_xbar = []
    while d <= TT:
        y_stream_temp = param.income_generator(D)

        #y_stream_temp = y_stream(Dlo:d)

        all_periods.append(d)

        y_hist = np.append(y_hist, y_stream_temp)

        # Save the consumption rule and parameters used:
        cons = LinearConsumpRule(gamma=gamma_xbar[0], mbar=gamma_xbar[1], ID=gamma_xbar, C0=EY)
        consumption_rules.append(deepcopy(cons))
        params.append(deepcopy(gamma_xbar))


        # Steps:
        # 0. consump function choosen
        # find true value associated with consump function
        # find acrifice value
        # find learned-value
        #   * display both
        # learn new function
        #   * display last consump and current consump, and learn-values
        # repeat


        if auto_mids_bins:
            # TRY: y_hist vs y_stream_temp
            if auto_mids_bins_full_yhist:
                # TRY: y_hist, all of agent's experience
                mids, bins, weights = single_stream_value_pre_estimate(cons=cons,
                                        u=param.u, beta=param.beta,
                                        R=param.R, x0=x0,
                                        y_shock_stream=y_hist,
                                        Nbins=param.Nbins_for_regret_learning)
            else:
                # TRY: y_hist vs y_stream_temp
                mids, bins, weights = single_stream_value_pre_estimate(cons=cons,
                                        u=param.u, beta=param.beta,
                                        R=param.R, x0=x0,
                                        y_shock_stream=y_stream_temp,
                                        Nbins=param.Nbins_for_regret_learning)


        # Record the mids used:
        mids_list.append(deepcopy(mids))


        w = find_value_function(c=cons, R=param.R, beta=param.beta, v0=param.u,
                                u=param.u, shocks=param.Ypoints,
                                probs=param.Yprobs, m_grid=param.grid,
                                eps=param.tol, maxiter=5000, verbose=False)

        #eps = lambda x, vopt_invs=vopt_invs, w=w: x - vopt_invs(w(x))
        #eps_bar = np.mean(eps(flat_sim_m))

        eps_bar = calculate_mean_sacrifice_value(vinv=vopt_invs,
                                                 vhat=w,
                                                 xvals=sim_m_mids,
                                                 xprobs=sim_m_probs)

        print "Sacrifice value for d =",d,"=",eps_bar
        eps_vals.append(eps_bar)


        # Plot two value functions:

        # Adjust value_plotgrid to not show too far down:
        v_plotgrid = plotgrid[plotgrid>=min(mids)]

        if all_sequential_plots:
            plt.plot(v_plotgrid, w(v_plotgrid), 'r-', label="Learned Value; Sacrifice = "+str(round(eps_bar,4)))
            plt.plot(v_plotgrid, optval(v_plotgrid), 'k-', label="True")
            plt.legend(loc='best', frameon=False)
            #plt.title(str(len)y_hist)) + ' periods total; step '+str(learn_counter)+' in learning with '+str(len(midsC))+' aggregated states')
            plt.title("Two value functions, period "+str(d)+"\nLength of learning: "+str(D)+" periods. Number of bins = "+str(len(mids)))
            plt.show()



        # vC_temp, xmids, c_stream_temp, x_stream_temp = single_stream_value_estimate(cons=cons,
        #         u=param.u, beta=param.beta, R=param.R, x0=x0, bins=bins, midpoints=mids, y_shock_stream=y_stream_temp, relative_value=param.relative_value)

        if use_full_yhist_for_value_learn:

            vC_temp, xmids, c_stream_temp, x_stream_temp = single_stream_value_estimate(cons=cons,
                    u=param.u, beta=param.beta, R=param.R, x0=x0,
                    bins=bins, midpoints=mids,
                    y_shock_stream=y_hist,
                    relative_value=param.relative_value)
        else:
            vC_temp, xmids, c_stream_temp, x_stream_temp = single_stream_value_estimate(cons=cons,
                    u=param.u, beta=param.beta, R=param.R, x0=x0,
                    bins=bins, midpoints=mids,
                    y_shock_stream=y_stream_temp,
                    relative_value=param.relative_value)


        if use_true_value:
            values_to_use = optval(xmids)
        else:
            values_to_use = vC_temp

        values_list.append(deepcopy(values_to_use))

        if param.EY_equals_mean == 1:
            EY_to_use = np.mean(y_hist)
        elif param.EY_equals_mean == 2:
            if use_full_yhist_for_value_learn:
                EY_to_use = np.mean(y_hist)
            else:
                EY_to_use = np.mean(y_stream_temp)
        else:
            EY_to_use = EY

        all_EY_to_use.append(EY_to_use)

        # Now look at the values we pull out
        if use_full_yhist_for_value_learn:
            mpc, xbar, regret_c, regret_x = regret_based_policy_update_reform_xstream(vvals=values_to_use,
                                                                  bins=bins,
                                                                  mids=mids, u=param.u,
                                                                  beta=param.beta,
                                                                  R=param.R,
                                                                  current_gamma_xbar=[cons.gamma, cons.mbar],
                                                                  EY=EY_to_use,
                                                                  x_stream=x_stream_temp,
                                                                  c_stream=c_stream_temp,
                                                                  y_stream=y_hist,
                                                                  x0=x0,
                                                                  sophisticated_regret=param.agents_sophisticated_regret,
                                                                  y_hist=y_hist,
                                                                  full_yhist=param.use_full_full_yhist_in_ecdf_vprime,
                                                                  approx_value=param.use_ecdf_to_get_vprime,
                                                                  use_ols_minimization=param.use_ols_minimization,
                                                                  trim_x=param.trim_x,
                                                                  trim_pctles=param.trim_pctles)
        else:
            mpc, xbar, regret_c, regret_x = regret_based_policy_update_reform_xstream(vvals=values_to_use,
                                                                  bins=bins,
                                                                  mids=mids, u=param.u,
                                                                  beta=param.beta,
                                                                  R=param.R,
                                                                  current_gamma_xbar=[cons.gamma, cons.mbar],
                                                                  EY=EY_to_use,
                                                                  x_stream=x_stream_temp,
                                                                  c_stream=c_stream_temp,
                                                                  y_stream=y_stream_temp,
                                                                  x0=x0,
                                                                  sophisticated_regret=param.agents_sophisticated_regret,
                                                                  y_hist=y_hist,
                                                                  full_yhist=param.use_full_full_yhist_in_ecdf_vprime,
                                                                  approx_value=param.use_ecdf_to_get_vprime,
                                                                  use_ols_minimization=param.use_ols_minimization,
                                                                  trim_x=param.trim_x,
                                                                  trim_pctles=param.trim_pctles)

        all_regret_c.append(deepcopy(regret_c))
        all_regret_x.append(deepcopy(regret_x))
        all_current_gamma_xbar.append(deepcopy([cons.gamma, cons.mbar]))
        #EY=np.mean(y_stream_temp)
        #params.append((mpc, xbar))
        revert_flag = False
        if mpc <=0:
            print "On step d=",d,"mpc=",mpc,"-- defaulting to previous mpc."
            revert_flag = True
            #mpc = params[-1][0]

        if xbar <=0:
            print "On step d=",d,"xbar=",xbar,"-- defaulting to previous xbar."
            revert_flag = True
            #xbar = params[-1][1]

        if revert_flag:
            if len(params) == 1:
                mpc = params[-1][0]  # Previous
                xbar = params[-1][1]
            elif len(params) == 2:
                mpc = 0.5*params[-1][0] + 0.5*params[-2][0]  # Convex combo of last two
                xbar = 0.5*params[-1][1] + 0.5*params[-2][1]
            else:
                mpc = 1.0/3*params[-1][0] + 1.0/3*params[-2][0] + 1.0/3*params[-3][0]  # Convex combo of last two
                xbar = 1.0/3*params[-1][1] + 1.0/3*params[-2][1] + 1.0/3*params[-3][1]

        new_gamma_xbar = (mpc, xbar)

        #learned_functions2.append( LinearConsumpRule(gamma=mpc, mbar=xbar, ID=(mpc, xbar), C0=EY) )
        max_c = np.maximum(max_c, np.max(regret_c))

        new_cons = LinearConsumpRule(gamma=mpc, mbar=xbar, ID=(mpc, xbar), C0=EY_to_use)

        if all_sequential_plots:
            # Plot the newest and previous consumption functions:
            plt.plot(plotgrid, cons(plotgrid), 'b--', label="previous cons")
            plt.plot(plotgrid, new_cons(plotgrid), 'b-', label="new cons")
            plt.plot(regret_x, regret_c, 'b*', label="regret choices n="+str(len(regret_c)))
            plt.plot(plotgrid, optcons(plotgrid), 'r--', label="True policy")
            plt.vlines(sim_mC_5_95_percentiles, ymin=0, ymax=1.2*max_c, linestyles='dotted', label='5th, 95th percentile')
            plt.legend(loc='best', frameon=False)
            plt.title("Learning Policy From Regret, period "+str(d)+"\nLength of learning: "+str(D)+" periods. Number of bins = "+str(len(mids)))
            plt.ylim(0, 2.0)
            plt.show()


        x0 = x_stream_temp[-1]  # Last value experienced...

        cons = new_cons
        gamma_xbar = new_gamma_xbar

        learn_counter += 1
        d += D



    # Now lets run a version where there are a lot of agents, same as value
    # estiamtion number. remeber to set up the initial m0 values correctly:

    m0_vals =  generate_simulated_shocks_discrete(mids, T=1, N=param.N_multiagent, seed=param.multiagent_RNG.randint(low=0,high=param.max_seed_int))

    # Start the sims:
    multiagent_simulation_vals = []
    for m in range(param.N_multiagent):
        m0_temp = m0_vals[0][m]

        # Create the new RNG for this agent.
        new_seed = param.multiagent_RNG.randint(low=0,high=param.max_seed_int)
        temp_RNG = np.random.RandomState(new_seed)

        temp_income_generator = lambda n, mu=param.mu, sig=param.sigma, RNG=temp_RNG: RNG.lognormal(mean=mu, sigma=sig, size=n)

        # Create the new agent, run the new agent, and collect the agent's values:
        agent_values = create_agent_run_1_learning_experience(x0=m0_temp,
                            D=param.D_autolearn,
                            TT=param.T_autolearn,
                            auto_mids_bins=param.auto_mids_bins,
                            auto_mids_bins_full_yhist = param.auto_mids_bins_full_yhist,
                            EY_equals_mean = param.EY_equals_mean,
                            shocks=param.shocks,
                            probs=param.probs,
                            income_generator=temp_income_generator,
                            u=param.u,
                            beta=param.beta,
                            R=param.R,
                            Nbins_for_regret_learning=param.Nbins_for_regret_learning,
                            relative_value=param.relative_value,
                            agents_sophisticated_regret=param.agents_sophisticated_regret,
                            use_full_full_yhist_in_ecdf_vprime=param.use_full_full_yhist_in_ecdf_vprime,
                            use_ecdf_to_get_vprime=param.use_ecdf_to_get_vprime,
                            use_ols_minimization=param.use_ols_minimization,
                            trim_x=param.trim_x,
                            trim_pctles=param.trim_pctles,
                            EY=None,
                            gamma_xbar=None,
                            mids=None,
                            bins=None,
                            weights=None)
        multiagent_simulation_vals.append(deepcopy(agent_values))


    # Now lets loop for all agents and put together snapshots of the parameters
    # used by the population of agents at present times.
    # ALSO, for all parameters used, find the sacrifice value!
    # and compile the massive list of those as well.
    # THEN LOOK AT the distribution for these things. Ask: "how does median consumer do in all
    # these situations?"


    '''
    # Now lets run a version where there are a lot of agents, same as value
    # estiamtion number. remeber to set up the initial m0 values correctly:

    m0_vals =  generate_simulated_shocks_discrete(mids, T=1, N=N_trial_agents, seed=param.example_single_long_income_seed)

    for m in range(_______):
        m0_temp = _______
        income_generator_temp = ________
        agent_values = create_agent_run_1_learning_experience(x0=m0_temp, D = param.D_autolearn, TT = param.T_autolearn, auto_mids_bins = param.auto_mids_bins, auto_mids_bins_full_yhist = param.auto_mids_bins_full_yhist, EY_equals_mean = param.EY_equals_mean, shocks=param.shocks, probs=param.probs, income_generator=income_generator_temp, u=param.u, beta=param.beta, R=param.R, Nbins_for_regret_learning=param.Nbins_for_regret_learning, relative_value=param.relative_value, agents_sophisticated_regret=param.agents_sophisticated_regret, use_full_full_yhist_in_ecdf_vprime=param.use_full_full_yhist_in_ecdf_vprime, use_ecdf_to_get_vprime=param.use_ecdf_to_get_vprime, use_ols_minimization=param.use_ols_minimization, trim_x=param.trim_x, trim_pctles=param.trim_pctles, EY=None, gamma_xbar=None, mids=None, bins=None, weights=None)

    # Now lets loop for all agents and put together snapshots of the parameters
    # used by the population of agents at present times.
    # ALSO, for all parameters used, find the sacrifice value!
    # and compile the massive list of those as well.
    # THEN LOOK AT the distribution for these things. Ask: "how does median consumer do in all
    # these situations?"

    '''

    # ------------------------------------------------------------------------ #
    # ------- Examine learning from own experience -- plot sacrifice values -- #
    # ------------------------------------------------------------------------ #

    # NOTE: these are taken from a similar codebase, which *already calculated*
    # these sacrifice values. Note that *should* move the code here, however.

    # Save values, because, come on...
    import cPickle

    with open('epsbar_mpc_mhats.pickle', 'rb') as f:
        my_vars = cPickle.load(f)
        epsbars = my_vars['epsbars']
        mpc_mhats = my_vars['mpc_mhats']

    # Set up the plot:
    import matplotlib

    matplotlib.rcParams['xtick.direction'] = 'in'
    matplotlib.rcParams['ytick.direction'] = 'in'

    V = [0.9, 0.75, 0.5, 0.4, 0.2, 0.05, 0.0075]

    if all_sequential_plots:
        plt.figure()
        #CS = plt.contour(X, Y, Z, V)
        CS = plt.contour(my_vars['X'], my_vars['Y'], my_vars['Z'], V, colors='k')
        plt.clabel(CS, inline=1, fontsize=10)
        #plt.title('Isosacrifice Contours')
        plt.xlabel(r"$\gamma$")
        plt.ylabel(r"$\bar{X}$")
        plt.text(x=0.167, y=1.18375, s='0.007', fontsize=10)
        plt.show()
        #plt.savefig("Isosacrifice Contours - epsilon bar.pdf")
        #plt.close()


    # ------------------------------------------------------------------------ #
    # ------- Plot learning on sacrifice values contour ---------------------- #
    # ------------------------------------------------------------------------ #


    # Grab the first N values of the learning process:
    N_params = len(params)
    gamma_vals = []
    xbar_vals = []
    for n in range(N_params):
        gamma_vals.append(params[n][0])
        xbar_vals.append(params[n][1])


    xrange = [0.975*min(np.min(gamma_vals), np.min(my_vars['X'])),  1.025*max(np.max(gamma_vals), np.max(my_vars['X']))]
    yrange = [0.975*min(np.min(xbar_vals), np.min(my_vars['Y'])),  1.025*max(np.max(xbar_vals), np.max(my_vars['Y']))]

    plt.figure()
    #CS = plt.contour(X, Y, Z, V)
    CS = plt.contour(my_vars['X'], my_vars['Y'], my_vars['Z'], V, colors='k')
    plt.clabel(CS, inline=1, fontsize=10)
    #plt.title('Isosacrifice Contours')
    plt.xlabel(r"$\gamma$")
    plt.ylabel(r"$\bar{X}$")
    plt.text(x=0.167, y=1.18375, s='0.007', fontsize=10)
    plt.xlim(xrange)
    plt.ylim(yrange)

    plt.plot(gamma_vals, xbar_vals, 'b-o')

    plt.show()








    '''

    # ------------------------------------------------------------------------ #
    # ------- Find the closest piecewise linear consumption function. -------- #
    # ------------------------------------------------------------------------ #

    # We will need the linear consumption function, welfare-finding function,
    # and minimizer.

    # sacrifice_value(cons, R, beta, u, shocks, probs, grid, vopt_invs)

    # Construct the sacrifice-value objective function:
    zx_vals = optval.get_coeffs()
    zy_vals = optval.get_knots()
    vopt_invs = InterpolatedUnivariateSpline(x=zx_vals, y=zy_vals, k=1)  # Skipping first because of...reasons
    #vopt_invs = InterpolatedUnivariateSpline(x=zx_vals[1:], y=zy_vals[1:], k=1)  # Skipping first because of...reasons

    def sacrifice_obj(gamma_mbar, R=param.R, beta=param.beta, u=param.u,
                      shocks=param.shocks, probs=param.probs, grid=param.grid,
                      tol=param.tol, vopt_invs=vopt_invs,
                      sim_m=flat_sim_m):
        # Create the consumption function from the parameters:
        cons = LinearConsumpRule(gamma=gamma_mbar[0], mbar=gamma_mbar[1])

        return sacrifice_value(cons=cons, R=R, beta=beta, u=u, shocks=shocks,
                               probs=probs, grid=grid, tol=tol,
                               vopt_invs=vopt_invs, simulated_x=sim_m)



    # use brute force:
    gamma_min = 0.0
    gamma_max = 2.0
    xbar_min = 0.0
    xbar_max = 3.0
    Ns = 5
    full_output =True

    Outbrute = brute(sacrifice_obj,
                    ((gamma_min, gamma_max),(xbar_min, xbar_max)),
                    Ns=Ns, full_output=full_output)
                    #finish=optimize.fmin)



    # Find the minimal-sacrifice-value linear rule:
    OUTPUT = fmin(sacrifice_obj, [1.0, 0.95], full_output=1)  # Start that the consume almost everything rule...

    xopt = OUTPUT[0]        # Parameter that minimizes function.
    fopt = OUTPUT[1]        # Value of function at minimum: ``fopt = func(xopt)``.
    optiter = OUTPUT[2]     # Number of iterations performed.
    funcalls = OUTPUT[3]    # Number of function calls made.
    warnflag = OUTPUT[4]    # warnflag : int
                            #   1 : Maximum number of function evaluations made.
                            #   2 : Maximum number of iterations reached.
    if warnflag != 0:
        warnings.warn("Minimization of sacrifice value failed! xopt, fopt, optiter, funcalls, warnflag = "+ str(xopt) +", "+ str(fopt) +", "+ str(optiter) +", "+ str(funcalls) +", "+ str(warnflagstr))

    best_linear_cons = LinearConsumpRule(gamma=xopt[0], mbar=xopt[1])

    # Plot the optimal consumption function against nearest linear approximation
    upperbound = np.ceil(np.max(optcons(param.grid)))

    plt.hist(flat_sim_m,normed=True, label="Cash on hand")
    plt.plot(param.grid, optcons(param.grid), 'r--', label="Optimal policy")
    plt.plot(param.grid, best_linear_cons(param.grid), 'r--', label="Best linear; sacrifice="+str(round(fopt,4)) )
    plt.vlines(sim_mC_5_95_percentiles, ymin=0, ymax=1.2*upperbound, linestyles='dotted', label='5th, 95th percentiles')
    plt.ylim(0, upperbound)
    plt.hold(True)
    plt.legend(loc='best', frameon=False)
    plt.title("True Optimal Consumption and\nDistribution of Cash-on-Hand")
    plt.show()

    '''

    # ------------------------------------------------------------------------ #
    # ------------ Plot sacrifice values for a wide range of rules ----------- #
    # ------------------------------------------------------------------------ #



    # WHERE AT: HERE!
    # TO-DO list for code, for today (17 Mar 2015)
    #   * plot sacrifice values
    #   * plot movement through sacrifice grid
    #   * try out agents who learn by doing own version of ECDF thing to create bins
    #   *
    #   *


    # "We now want to see how well an agent can learn a policy function from own
    # experience, starting from an arbit"




    # "Thus far this has all been preliminary examination. We have been
    # examining how well (or how poorly) an agent can learn a coarse
    # appromximation to the value function when he simply learns from a single
    # stream of income experience. We will show [see appendix] that learning
    # from a single stream will always be biased in finite time, but that this
    # bias goes to zero as we average over the values calculated from multiple
    # independent experiences. This is something of a "proof of concept" and a
    # demonstration of a variation on asynchronous dynamic programming.^[For a
    # thorough definition and discussion of asynchronous dynamic programming,
    # see my primer "____________."" For good source material, Sutton and Barto
    # (1999) provides an excellent introductory discussion, and Bertsekas (____)
    # examines the convergence properties of asynchronous DP in great detail.]
    #
    # Our interest, however, is not how well an agent can value the optimial
    # policy function from a single stream of experience (as interesting as the
    # mathematical aspects of this question are). Rather, we care about
    # whether (and how well) an agent can use his single stream of income shocks
    # to learn a near-optimal consumption function when he *starts from any
    # arbitrary consumption function*.
    #
    # It turns out that an agent *can* learn a near-optimal policy from
    # experience alone, and in reasonable time.^[To be defined below.] A key
    # element of this learning process is *regret*: an agent will try an
    # arbitrary rule for a period of time. At a point the agent looks back over
    # the previous experience and essentially asks the question, "if I could go
    # back, knowing what I know now, what *should* I have done?" The "regret"
    # choices, which answer this question, form the basis for the next
    # consumption function the agent employs. The agent will try this new rule
    # for a period of time, stop, and repeat the "regret" process. We will see
    # that this quickly brings the agent very close to the true optimal
    # consumption function, in welfare terms.
    #
    # Here is how the agent will proceed: Choose a "regret horizon," D, and a
    # number of "bins," B, to parititon the continuous state space into. The
    # agent starts with an initial consumption function, initial wealth m0,
    # preference parameters beta and gamma, return on assets R, an income
    # process, and the values D and B. An equiprobably parition of the state
    # space is constructed for the agent with appropriately selected midpoints,
    # such that the midpoints represent what would be the nodes of a simple
    # discretization of the ergodic distribution of cash-on-hand, under the
    # true optimal rule.^[In the electronic appendix I explore results when this
    # grid is continually adjusted to represent the ergodic distribution of the
    # currently used consumption function when it exists, or alternatively,
    # the equiprobable discretization employing the ecdf formed by considering
    # only the agent's experience under the current rule. Results and
    # differences in these results are discussed.]
    #
    # The agent then experiences a string of income shocks of length D and
    # incrementally updates the simple estimate of the value function associated
    # with each parition, as discussed in section ****X**** below. This
    # estimation process represents the one place I must "fudge" the agent's
    # abilities. To undertake the estimation, the agent must maintain two
    # vectors in memory:
    #   * a 1xB vectors of hypothetical cash-on-hand values, one for each bin,
    #   * a 1xB vector of values for each partition.
    #
    # Furthermore, to undertake "regret" learning, the agent must in addition
    # recall the following values:
    #   * a 1xD vector of states actually visited following the cons rule, and
    #   * a 1xD vector of the income shocks experienced during this trial.
    # This is
    # update
    # a single 1xB vector of cash-on-hand associated with each track
    # a vector of "is the one place I ] by each
    # of




    #
    #
    # To the reader familiar with policy iteration, this process will be very
    # familiar: the agent is simply enacting generalized policy iteration^[See
    # Sutton and Barto (1999), Chapter _, for an excellent introductory
    # discussion] such that the value estimation step is not carried through
    # to completion.^[In the spectrum described by Sutton and Barto (1999), this
    # quite literally falls somewhere between pure value iteration, on the one
    # hand, and pure policy iteration on the other.]
    #
    # Recall^[from the appendix] that asynchronous dynamic programming
    # essentially replaces a convergence criteria with a length of experience.
    # In a sense, this is a very particular version of asynchronous policy
    # iteration. A convergence criteria is replaced with a length of learning
    # time, and the consumption function is approximated with an intuitive,
    # two-parameter linear approximation to the true form.
    #
    # An important implication of this approach is that there are only two new
    # parameters to consider for the learning agent: the length of time the
    # agent tries a rule before "regretting" past decisions (finding a new
    # rule), and the "coarseness" of the grid the agent uses to approximate the
    # value of a particular rule.
    #
    # There are two ways to think about choosing these values:
    #   (1) choose (D,B) to minimize welfare loss during learning for different
    #       horizons T.
    #   (2) choose (D,B) to minimize the distance between structural simulation
    #       output and empirical data.
    #
    # Perhaps jointly fit beta and gamma as well. Then the real question is how
    # well the (D,B) values match the values from what people appear to be doing
    # ... and how well the "best jointly fit" (beta, gamma) values from leanring
    # fit from non-learning.
    #
    # HMMMMMMMM.  Figuring out how to bring this to data would be *very*
    # interesting and tricky. But super interesting!
    #
    # The coarseness question may very well be answered when considering the
    # tradeoff between the
    #
    #
    # is an extremely important fea for the consumption problem we consider, that
    # optimal consumption function from experience alone.  but rather how well an
    # agent using a single stream of
    # We would like for an agent to start with an arbitrary
    # However, even in dynamic programming, an individual agent does not need to
    # exactly estimate a value function for  function before
    #
    #
    # "If the reader is familiar with Q-learning, you may recognize this as an
    # effort to retain the approach of Q-learning, but doing away with the need
    # for remembering both state and optimal action. We essetially back out the
    # action from the states experienced..."
    #
    # "I am looking for the *simplest* structural version of learning which will
    # still give me convergence to the optimium. There are obvjous extensions,
    # hower the suprising thing is that the optimum can still be found in
    # relatively little time, with relatively few mechanics required."
    #
    #
    # "THE KEY is thinking about the FULL COST of learning: the LOSS DUE TO
    # excessive exploration is *NON-TRIVIAL,* and AGENTS DIE QUICKLY. THUS, the
    # IMPORTANT KEY ELEMENT is "
    #
    # "This is simply a way to frame learning from first principles. The
    # advantages are that it suggests a structured way tothink about the true
    # and non-trivial costs of optimizing a diffcult problem. It also,
    # somewhat surprisingly, demosntrates that appropriately "coarsifying" the
    # problem can greatly reduce the inherent lifetime loss due to exploration.
    # It also introduces a structured way to think about agents "making mistakes"
    # (NOTE: need to DEMONSTRATE some of these, or people will not be able to
    # connect with them!).
    # When we think of positively [normatively?] modeling dynamic agent "
    #
    # This suggests a number of things:
    #   1. first-principles way to frame learning
    #   2. 1-prins way to think about true cost of learning to optimize
    #   3. 1-prins way to think about ways agents may "make mistakes."
    #   4.
    #   5.
    #   6.
    #   7.
    #
    # Three ways to think about this:
    #
    #   2. a way to model *how agents learn* in simplest wya possible
    #       - and frame out stats and analy
    #       * organize the problem in well-stated mathematical framework
    #   3. normative: try to model *how things are* - rational econometrician -
    #       we *need* to be able to model population as convex combo of perhaps
    #       3 types of agents -- "near rational", replicators, "unsure", others?
    #       EMPIRICAL QUESTION as to how to capture learning best!
    #       "Properly capturing learning is the most important structural
    #       question we have in modeling agents..."
    #
    #   4. possitive: can we identify what elements may be *most helpful* for
    #       accelerating agent learning? can we ID the best ways to simplify
    #       and approximate the problem space so agents can *solve problem
    #       efficiently?* (EG: this only possible because we *first* identified
    #       the simple approximation *form* which could get extremely close to
    #       the optimal rule -- first! Then learned more about how quickly could
    #       learn...)  Once we have IDed forms, we can FURTHERMORE ID which
    #       aspects of those forms are most important in welfare terms. (Params! mbar vs mpc!!!)
    #
    #   5. STILL important Qs about soc learning, effects on GE! There are almost
    #       cetainly externalities in learning! MUST understand these dynamics!
    # function when All of this has so far been an agent learning the optimal value,
    # conditional on having the optimal policy function in hand. We want to
    # exami

    # NOTE: Items that need to be discussed to really explain what is happening:
    #
    #   1.  Carefully outline the First-Visit Monte Carlo value estimation.
    #       ("This is the absolute simplest/most straightforward of RL value
    #       estimation algos...")
    #
    #   2.  Outline how "appropriate usage of FV MC" gets us to a the true value
    #       function.
    #       Then compare value functions estimated on a grid:
    #           a.  select large set of y-shocks
    #           b.  use them to estimate traditional FV MC value function
    #           c.
    #       using *only* the randomlay
    #
    #


    '''
    Num_bins = 5

    mids, bins, weights = find_empirical_equiprobable_bins_midpoints(N=Num_bins, data=sim_mC.ravel())

    midsD, binsD, weightsD = find_empirical_equiprobable_bins_midpoints(N=Num_bins, data=sim_mD.ravel())

    y_stream = income_generator(10000)

    sim_mC_5_95_percentiles = np.percentile(sim_mC.ravel(), [5.0, 95.0])
    '''

#
