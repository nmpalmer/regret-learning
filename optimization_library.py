'''
optimization_library.py

This file contains the library of functions necessary to execute a variety of
solutions to dynamic optimization problems under uncertainty. These dynamic
programming methods find the true optimal solutions within some epsilon error.
These are not learning methods which get within some welfare-distance of the
true optimal policy with some probability.

The current library solves the simple 1-state, 1-shock version of these dynamic
optimization problems. Future libraries will contain extended versions which
take multiple states and / or multiple shocks.

At the moment the functions included still largely need formal testing. They
have been informally tested in the file

    comparing_all_dynamic_programming_approaches__from_all_midpoints_social_learning.py

but a formal testing framework is still in progress.

Create a checklist below and check off each function as it is tested -- often,
at beast, against other different iterations and approaches to solving the same
problem (since most problems do not have a closed-form solution).


By Nathan M. Palmer
On 27 Feb 2015
'''
from __future__ import division
import numpy as np
import pylab as plt
from copy import deepcopy
from scipy.optimize import fminbound
from scipy.interpolate import InterpolatedUnivariateSpline




def value_estimation(v, u, f, beta, grid, tol, Ypoints, Yprobs):
    """
    New VI algorithm based on other code.

    Needs to be tested!
    """

    # IF v is None, set v = 0:
    if v is None:
        v = InterpolatedUnivariateSpline(x=grid, y=np.zeros_like(grid), k=1)

    grid_min = np.min(grid)
    c_prime = np.empty_like(grid)
    v_prime = np.empty_like(grid)
    error = tol + 1
    pol_list = [ ]
    val_list = [ ]
    while error > tol:

        for i, x in enumerate(grid):

            def H(c):
                vprime  = lambda z: v(f(x,c,z))
                return -1*(u(c) + beta*np.dot(vprime(Ypoints), Yprobs))

            #c_prime[i] = fminbound(H, grid_min, x)
            xopt, fval, ierr, numfunc = fminbound(H, grid_min, x, full_output=1)
            if ierr !=0:
                # Then we have not converged.
                warnings.warn("In function T, fminbound returned ierr="+str(ierr)+". numfunc="+str(numfunc))
            c_prime[i] = xopt
            v_prime[i] = -fval
        # Update the error
        error = max(np.abs(v_prime - v(grid)))
        # Update the value function
        v = InterpolatedUnivariateSpline(x=grid, y=v_prime, k=1)
        # Update the policy function
        policy = InterpolatedUnivariateSpline(x=grid, y=c_prime, k=1)

        pol_list.append(deepcopy(policy))
        val_list.append(deepcopy(v))

    # At end of the iterations return the values, etc
    return policy, v, pol_list, val_list


def gpi_policy_improvement(v, u, f, beta, grid, tol, Ypoints, Yprobs):
    """
    Obtain a greedy policy given a value function.

    Taken (and minimally modified) from the file:
        ~/workspace_1/dynamic-optimization/continuous_state/dynamic_programming/sutton_barto_gpi.py

    # Parameters:
    * v: value function
    * u: utility function
    * f: law of motion for state
    * beta: discount factor
    * grid: array of grid points
    * tol:  stopping tolerance
    * Ypoints: array of discrete probability points
    * Yprobs: probs for above points. Should sum to 1.

    Original by Nathan M. Palmer
    On June 2013
    """
    grid_min = np.min(grid)
    c_prime = np.empty_like(grid)
    for i, x in enumerate(grid):

        def H(c):
            vprime  = lambda z: v(f(x,c,z))
            return -1*(u(c) + beta*np.dot(vprime(Ypoints), Yprobs))

        c_prime[i] = fminbound(H, grid_min, x)

    return(InterpolatedUnivariateSpline(x=grid, y=c_prime, k=1))


def gpi_value_estimation(v, policy, u, f, beta, grid, tol, Ypoints, Yprobs):
    """
    Iterate to a fixed point value function, given a value function v and
    a policy.

    Taken (and minimally modified) from the file:
        ~/workspace_1/dynamic-optimization/continuous_state/dynamic_programming/sutton_barto_gpi.py

    # Parameters:
    * v: value function
    * policy: policy (action) function
    * u: utility function
    * f: law of motion for state
    * beta: discount factor
    * grid: array of grid points
    * tol:  stopping tolerance
    * Ypoints: array of discrete probability points
    * Yprobs: probs for above points. Should sum to 1.

    Original by Nathan M. Palmer
    On June 2013
    """

    error = tol + 1
    new_v = np.empty_like(grid)
    while error > tol:
        for i, x in enumerate(grid):
            c = policy(x)
            vprime  = lambda z: v(f(x,c,z))
            new_v[i] = u(c) + beta* np.dot(vprime(Ypoints), Yprobs)
        error = max(np.abs(new_v - v(grid))) #max(np.abs(new_v[1:] - v(grid[1:])))
            # Ignore the first element. Errors often due to CRRA utility
            # function, which is unbounded below/above (depending on r.a.).

        v = InterpolatedUnivariateSpline(x=grid, y=new_v, k=1)

    return(v)

def opi_value_estimation(v, policy, m, u, f, beta, grid, Ypoints, Yprobs):
    """
    Optimistic policy iteration, evaluation step: Iterate m times to get to a
    new value function, given a value function v and a policy.

    This is the "intermediate" step between value iteration, policy iteration.

    # Parameters:
    * v: value function
    * policy: policy (action) function
    * m: number of intermediate steps to take.
    * u: utility function
    * f: law of motion for state
    * beta: discount factor
    * grid: array of grid points
    * Ypoints: array of discrete probability points
    * Yprobs: probs for above points. Should sum to 1.

    Original by Nathan M. Palmer
    On June 2013
    """

    #error = tol + 1
    new_v = np.empty_like(grid)
    for k in range(m):
        for i, x in enumerate(grid):
            c = policy(x)
            vprime  = lambda z: v(f(x,c,z))
            new_v[i] = u(c) + beta* np.dot(vprime(Ypoints), Yprobs)

        v = InterpolatedUnivariateSpline(x=grid, y=new_v, k=1)

    return(v)

def opi_value_estimation_vector(v_vec, policy, m, u, f, beta, grid, Ypoints, Yprobs):
    """
    Optimistic policy iteration, evaluation step: Iterate m times to get to a
    new value function, given a value function v and a policy.

    This is the "intermediate" step between value iteration, policy iteration.

    # Parameters:
    * v_vec: vector containing value function values on the grid
    * policy: policy (action) function
    * m: number of intermediate steps to take.
    * u: utility function
    * f: law of motion for state
    * beta: discount factor
    * grid: array of grid points
    * Ypoints: array of discrete probability points
    * Yprobs: probs for above points. Should sum to 1.

    Original by Nathan M. Palmer
    On June 2013
    """

    v = InterpolatedUnivariateSpline(x=grid, y=v_vec, k=1)
    new_v = np.empty_like(grid)
    for k in range(m):
        for i, x in enumerate(grid):
            c = policy(x)
            vprime  = lambda z: v(f(x,c,z))
            new_v[i] = u(c) + beta* np.dot(vprime(Ypoints), Yprobs)

        v = InterpolatedUnivariateSpline(x=grid, y=new_v, k=1)

    return(new_v)



def gpi_value_estimation_stochastic_error(v, policy, u, f, beta, grid, tol, Ypoints, Yprobs):
    """
    Iterate to a fixed point value function, given a value function v and
    a policy.

    Allow for a stochastic error on consumption choice. Once consumption choice
    is made, add mean-0 normal noise with some variance sigma^2.

    Taken (and minimally modified) from the file:
        ~/workspace_1/dynamic-optimization/continuous_state/dynamic_programming/sutton_barto_gpi.py

    # Parameters:
    * v: value function
    * policy: policy (action) function
    * u: utility function
    * f: law of motion for state
    * beta: discount factor
    * grid: array of grid points
    * tol:  stopping tolerance
    * Ypoints: array of discrete probability points
    * Yprobs: probs for above points. Should sum to 1.

    Original by Nathan M. Palmer
    On June 2013
    """

    error = tol + 1
    new_v = np.empty_like(grid)
    while error > tol:
        for i, x in enumerate(grid):
            c = policy(x)
            # Now determine the distrib of c-errors:
            # TODO: fill this out. ANd ask around...
            vprime  = lambda z: v(f(x,c,z))
            new_v[i] = u(c) + beta* np.dot(vprime(Ypoints), Yprobs)
        error = max(np.abs(new_v - v(grid))) #max(np.abs(new_v[1:] - v(grid[1:])))
            # Ignore the first element. Errors often due to CRRA utility
            # function, which is unbounded below/above (depending on r.a.).

        v = InterpolatedUnivariateSpline(x=grid, y=new_v, k=1)

    return(v)


def generalized_policy_iteration(u, f, beta, grid, tol, Ypoints, Yprobs, v=None,
                                 policy=None, verbose=False):
    """
    This is the generalized policy function iteration code I created from
    the Sutton and Barto textbook. Included here as a comparison.

    Note: while this nails the policy function, it doesn't appear to nail
    the value function -- for some reason, just a little off. Huh.

    Taken (and minimally modified) from the file:
        ~/workspace_1/dynamic-optimization/continuous_state/dynamic_programming/sutton_barto_gpi.py

    ----------------------------------------------------------------------------
    This is brought straight from dynamic_optimization library, with minimal
    changes (changes only as needed to make it function outside the classes in
    that directory).

    Original by Nathan M. Palmer
    On June 2013

    """

    if not(v):
        v = InterpolatedUnivariateSpline(x=grid, y=u(grid), k=1)

    if not(policy):
        policy = InterpolatedUnivariateSpline(x=grid, y=grid, k=1)
            #45 deg line.

    first_time = True
    delta = tol*2+1

    while delta > tol:

        if verbose:
            plt.plot(grid, v(grid), 'bo-')
            print "tol = ", tol, "delta = ", delta

        # Given a value function and a policy function, improve the policy:
        policy = gpi_policy_improvement(v, u, f, beta, grid, tol, Ypoints, Yprobs)

        # Estimate the value function:
        v_prime = gpi_value_estimation(v, policy, u, f, beta, grid, tol, Ypoints, Yprobs)

        # Examine the difference between old value, new value:
        delta = np.max(np.abs( v_prime(grid) - v(grid) ))

        # Update value function:
        v = deepcopy(v_prime)

    if verbose:
        plt.show()

        plt.plot(grid, v(grid), 'bo-')
        plt.show()

        plt.plot(grid, policy(grid), 'ro-')
        plt.show()

    return(policy, v_prime)



def optimistic_policy_iteration(u, f, m, beta, grid, tol, Ypoints, Yprobs, v=None,
                                 policy=None, verbose=False):
    """
    This is the optimistic policy function iteration code created from the
    generalized policy iteration code above.

    Note: while this nails the policy function, it doesn't appear to nail
    the value function -- for some reason, just a little off. Hmmm.

    ----------------------------------------------------------------------------
    This is brought straight from dynamic_optimization library, with minimal
    changes (changes only as needed to make it function outside the classes in
    that directory).

    Original by Nathan M. Palmer
    On June 2013
    Updated 10Sep2015
    """

    if not(v):
        v = InterpolatedUnivariateSpline(x=grid, y=u(grid), k=1)

    if not(policy):
        policy = InterpolatedUnivariateSpline(x=grid, y=grid, k=1)
            #45 deg line.

    first_time = True
    delta = tol*2+1

    while delta > tol:

        if verbose:
            plt.plot(grid, v(grid), 'bo-')
            print "tol = ", tol, "delta = ", delta

        # Given a value function and a policy function, improve the policy:
        policy = gpi_policy_improvement(v, u, f, beta, grid, tol, Ypoints, Yprobs)

        # Estimate the value function:
        v_prime = opi_value_estimation(v=v, policy=policy, m=m, u=u, f=f,
                                       beta=beta, grid=grid, Ypoints=Ypoints,
                                       Yprobs=Yprobs)

        # Examine the difference between old value, new value:
        delta = np.max(np.abs( v_prime(grid) - v(grid) ))

        # Update value function:
        v = deepcopy(v_prime)

    if verbose:
        plt.show()

        plt.plot(grid, v(grid), 'bo-')
        plt.show()

        plt.plot(grid, policy(grid), 'ro-')
        plt.show()

    return(policy, v_prime)




def generalized_policy_iteration_all_fxns(u, f, R, beta, grid, tol, Ypoints, Yprobs, v=None,
                                 policy=None, verbose=False):
    """
    This is the generalized policy function iteration code I created from
    the Sutton and Barto textbook. Included here as a comparison.

    Note: while this nails the policy function, it doesn't appear to nail
    the value function -- for some reason, just a little off. Huh.

    Taken (and minimally modified) from the file:
        ~/workspace_1/dynamic-optimization/continuous_state/dynamic_programming/sutton_barto_gpi.py

    ----------------------------------------------------------------------------
    This is brought straight from dynamic_optimization library, with minimal
    changes (changes only as needed to make it function outside the classes in
    that directory).

    Original by Nathan M. Palmer
    On June 2013

    """

    if not(policy):
        policy = InterpolatedUnivariateSpline(x=grid, y=grid, k=1)
            #45 deg line.
    if not(v):
        # Pull v from the policy:
        v0 = InterpolatedUnivariateSpline(x=grid, y=u(grid), k=1)
        v  = find_value_function(c=policy, R=R, beta=beta, v0=u, u=u,
                    shocks=Ypoints, probs=Yprobs, m_grid=grid)


    first_time = True
    delta = tol*2+1

    val_list = [deepcopy(v)]
    pol_list = [deepcopy(policy)]


    while delta > tol:

        if verbose:
            plt.plot(grid, v(grid), 'bo-')
            print "tol = ", tol, "delta = ", delta

        # Given a value function and a policy function, improve the policy:
        policy = gpi_policy_improvement(v, u, f, beta, grid, tol, Ypoints, Yprobs)

        # Estimate the value function:
        v_prime = gpi_value_estimation(v, policy, u, f, beta, grid, tol, Ypoints, Yprobs)

        val_list.append(deepcopy(v_prime))
        pol_list.append(deepcopy(policy))

        # Examine the difference between old value, new value:
        delta = np.max(np.abs( v_prime(grid) - v(grid) ))

        # Update value function:
        v = deepcopy(v_prime)

    if verbose:
        plt.show()

        plt.plot(grid, v(grid), 'bo-')
        plt.show()

        plt.plot(grid, policy(grid), 'ro-')
        plt.show()

    return(policy, v_prime, pol_list, val_list)



def optimistic_policy_iteration_all_fxns(u, f, m, beta, grid, tol, Ypoints, Yprobs, v=None,
                                 policy=None, verbose=False):
    """
    This is the optimistic policy function iteration code created from the
    generalized policy iteration code above.

    Note: while this nails the policy function, it doesn't appear to nail
    the value function -- for some reason, just a little off. Hmmm.

    ----------------------------------------------------------------------------
    This is brought straight from dynamic_optimization library, with minimal
    changes (changes only as needed to make it function outside the classes in
    that directory).

    Original by Nathan M. Palmer
    On June 2013
    Updated 10Sep2015
    """

    if not(v):
        v = InterpolatedUnivariateSpline(x=grid, y=u(grid), k=1)

    if not(policy):
        policy = InterpolatedUnivariateSpline(x=grid, y=grid, k=1)
            #45 deg line.

    first_time = True
    delta = tol*2+1

    val_list = [deepcopy(v)]
    pol_list = [deepcopy(policy)]

    while delta > tol:

        if verbose:
            plt.plot(grid, v(grid), 'bo-')
            print "tol = ", tol, "delta = ", delta

        # Given a value function and a policy function, improve the policy:
        policy = gpi_policy_improvement(v, u, f, beta, grid, tol, Ypoints, Yprobs)

        val_list.append(deepcopy(v))
        pol_list.append(deepcopy(policy))

        # Estimate the value function:
        v_prime = opi_value_estimation(v=v, policy=policy, m=m, u=u, f=f,
                                       beta=beta, grid=grid, Ypoints=Ypoints,
                                       Yprobs=Yprobs)

        # Examine the difference between old value, new value:
        delta = np.max(np.abs( v_prime(grid) - v(grid) ))

        # Update value function:
        v = deepcopy(v_prime)

    if verbose:
        plt.show()

        plt.plot(grid, v(grid), 'bo-')
        plt.show()

        plt.plot(grid, policy(grid), 'ro-')
        plt.show()

    return(policy, v_prime, pol_list, val_list)





def find_value_function(c, R, beta, v0, u, shocks, probs, m_grid, eps=1e-8, maxiter=5000, verbose=False, relative_value=False):
    '''

    Arguments:
    * relative_value: boolean
        * if true, find_value_function will *also* estimate the "spendthrift"
          value function and subtract that from the "regular" value function.
    '''

    # NOTE: should probably re-code this to use a generic transition function,
    # instead of hard-coding one in there.


    #raise Exception, "Not ready to use yet; still in vectorization process."

    v = deepcopy(v0)
    abs_diff = 3*eps
    ctr=0

    # Quick check for lower grid bound:
    #if m_grid[0] == 0:
    #    m_grid[0] = m_min
    vold = v0(m_grid)
    while abs_diff >= eps and ctr < maxiter:
        vm = []

        for m in m_grid:

            # NOTE: the way to vectorize this is to split the z-values (and pz-vals)
            # along a seperate axis -- i.e. set up a meshgrid type thing. Then
            # just apply everything as usual along the grid, then collapse back into
            # a 1D grid via dot-ing (mult then summing) along the z/pz axis.

            g = lambda z, m_=m, c_=c, R_=R, v_=v: v_(R_*(m_-c_(m_)) + z)
            vm.append( u(c(m)) + beta * np.dot(g(shocks), probs))
            #vm.append( u(c(m)) + beta * math.fsum(g(shocks)*probs))
        vm = np.array(vm)
        abs_diff = np.max(np.abs(vold - vm))
        vold = vm
        v = InterpolatedUnivariateSpline(m_grid, vm, k=1)
        ctr += 1

        if ctr%25 == 0 and verbose:
            print "On step", ctr

        if ctr >= maxiter:
            raise Exception, "ctr hit maxiter value of "+ str(maxiter) +" before converging.\nabs_diff = "+ str(abs_diff) +" > eps = "+ str(eps)

    if relative_value:
        save_v = deepcopy(vm)  # Save the "levels"/"regular?" value
        # Then *also* estimate the value for the "spendthrift" value function.
        # Essentially repeat everything above. Simplest first version: copypaste
        # values above.
        print "Estimating spendthrift rule."
        v = deepcopy(v0)
        abs_diff = 3*eps
        ctr=0

        # Quick check for lower grid bound:
        #if m_grid[0] == 0:
        #    m_grid[0] = m_min
        vold = v0(m_grid)
        while abs_diff >= eps and ctr < maxiter:
            vm = []

            for m in m_grid:

                # NOTE: the way to vectorize this is to split the z-values (and pz-vals)
                # along a seperate axis -- i.e. set up a meshgrid type thing. Then
                # just apply everything as usual along the grid, then collapse back into
                # a 1D grid via dot-ing (mult then summing) along the z/pz axis.

                g = lambda z, m_=m, c_=c, R_=R, v_=v: v_(R_*(m_-c_(m_)) + z)
                vm.append( u(c(m)) + beta * np.dot(g(shocks), probs))
                #vm.append( u(c(m)) + beta * math.fsum(g(shocks)*probs))
            vm = np.array(vm)
            abs_diff = np.max(np.abs(vold - vm))
            vold = vm
            v = InterpolatedUnivariateSpline(m_grid, vm, k=1)
            ctr += 1

            if ctr%25 == 0 and verbose:
                print "On step", ctr

            if ctr >= maxiter:
                raise Exception, "ctr hit maxiter value of "+ str(maxiter) +" before converging.\nabs_diff = "+ str(abs_diff) +" > eps = "+ str(eps)

    return v


def sacrifice_value(cons, R, beta, u, shocks, probs, grid, tol, vopt_invs, simulated_x):
    w = find_value_function(c=cons, R=R, beta=beta, v0=u, u=u, shocks=shocks, probs=probs, m_grid=grid, eps=tol, maxiter=5000, verbose=False)

    eps = lambda x, vopt_invs=vopt_invs, w=w: x - vopt_invs(w(x))
    eps_bar = np.mean(eps(simulated_x))
    return eps_bar


# The code below is an implementation of the solution methods in
# Ljungqvist and Sargent, Recursive Macroeconomics, 3rd Ed.
# The goal of this implementation is fleshing out the math.
# NOTE:


def T(w, grid, u, beta, f, compute_policy=False, eps=1e-6):
    """
    The approximate Bellman operator, which computes and returns the
    updated value function Tw on the grid points.

    Parameters
    ----------
    w : value function.
        A continuous function. Can be linearly interpoated function.
    compute_policy : Boolean, optional(default=False)
        Whether or not to compute policy function


    NOTE: This function is taken directly from quant-econ.net, the
    Infinite Horizon Dynamic Programming chapter. It has been modified for my
    puposes in replicating the dynamic programming proofs in Appendix A.
    At the time of this writing, this chapter can be found at this address:

        http://quant-econ.net/py/dp_intro.html

    Note that, *very specifically,* I am attempting to replicate the steps in
    the apendix for policy iteration.

    Nathan Palmer
    09Aug2015
    """
    # === Apply linear interpolation to w === #
    # Aw = lambda x: interp(x, self.grid, w)    # Assume this is already done.
    Aw = w

    if compute_policy:
        sigma = np.empty(len(w))

    # == set Tw[i] equal to max_c { u(c) + beta w(f(k_i) - c)} == #
    Tw = np.empty(len(w))
    for i, k in enumerate(grid):
        objective = lambda c: - u(c) - beta * Aw(f(k) - c)
        xopt, fval, ierr, numfunc = fminbound(objective, eps, f(k), full_output=1)
        if ierr !=0:
            # Then we have not converged.
            warnings.warn("In function T, fminbound returned ierr="+str(ierr)+". numfunc="+str(numfunc))
        c_star = xopt
        if compute_policy:
            # sigma[i] = argmax_c { u(c) + beta w(f(k_i) - c)}
            sigma[i] = c_star
        Tw[i] = - fval #objective(c_star)

    if compute_policy:
        return Tw, sigma
    else:
        return Tw


def T_mu(w, grid, u, beta, f, compute_policy=False, eps=1e-6):
    '''
    A single iteration of the T mapping. See docs for T.
    '''
    return T(w, grid, u, beta, f, compute_policy, eps)
