'''
regret_learning_agent.py

By Nathan M. Palmer
Updates: 12 Mar 2015
'''
from __future__ import division
import numpy as np
import pylab as plt
from time import time
from copy import deepcopy
from progress_bar import ProgressBar
from datetime import datetime, timedelta
from statsmodels.tools.tools import ECDF
from scipy.stats.mstats import mquantiles
from HACKUtilities import calculate_mean_sacrifice_value, warnings # warnings library has been slightly modified.
from scipy.interpolate import InterpolatedUnivariateSpline
from optimization_library import generalized_policy_iteration, find_value_function, opi_value_estimation_vector
from simulation_library import find_empirical_equiprobable_bins_midpoints, generate_simulated_choice_data_continuous
from learning_library import single_stream_first_visit_full_value_estimate, find_error_minimizing_consumption_ols, update_policy_by_regret_ecdf

import dill as pickle
import multiprocessing
from joblib import Parallel, delayed
from functools import total_ordering

'''
>>> from math import sqrt
>>> from joblib import Parallel, delayed
>>> Parallel(n_jobs=2)(delayed(sqrt)(i ** 2) for i in range(10))
[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0]
'''


@total_ordering
class RegretAgent(object):

    def __lt__(self, other):
        return (self.i < other.i)

    def __init__(self, u, beta, a0, D, T, EY0, mpc0, mbar0, N, i, mtilde=0.0,
                 record_regret_c_m=False):

        self.i = i
        self.u = u          # If needed for speed, simply pass in param
        self.beta = beta    # Discount factor
        self.a0 = a0        # Initial end-of-period assets ("savings") allocation
        self.D = D          # Learning length. NOTE: can replace with Poisson
        self.T = T          # Final period
        self.learn_periods = np.arange(D,T,D)   # Periods at which learning
                                # occurs. If Poisson learning is desired simply
                                # replace this vector. See "step" below.
        self.prev_learn_t = 0   # Previous period at which learning occurred
        self.mpc = mpc0         # Intial consumption mpc
        self.mbar = mbar0       # Initial consumption buffer target
        self.EY = EY0           # Initial expected income
        self.w = None           # Initial value estimate
        self.N = N              # Number of partitions for value estimate

        # NOTE: NEED TO INSERT "ASSERT" statement here that lets me check that
        # the appropriate conditions are met!


        self.mtilde = mtilde    # The borrowing constraint for the agent.
                                # Should be negative for allowing borrow against
                                # the future.
                                # TODO: Confirm that this is is done corectly.
        self.record_regret_c_m = record_regret_c_m
        # Containers for the values.
        # Note: start with a "None" to align these with the parameters. There
        # are no regret-c, actual-m, or m-grid values associated with the First
        # initial set of parameters.
        self.regret_c_by_episode = [None]   # regret values for a learning period
        self.actual_c_by_episode = [None]   # actually-experienced c-values for an episode
        self.actual_m_by_episode = [None]   # actually-experienced m-values for an episode
        self.actual_y_by_episode = [None]
        self.actual_m_by_episode_compare = [None]
        self.m_grid_bins_by_episode = [None]
        self.m_grid_mids_by_episode = [None]
        self.valsB_by_episode = [None]


        if mtilde != 0:
            warnings.warn("WARNING: The non-0 borrowing constraint has not yet been implemented; simulation will run with mtilde=0!")
            # TO implement, need to ensure that all versions of the consumption
            # function in this code are set appropriately, all versions of the
            # resource constraint are set properly, and all versions of the
            # "parameter check" method are set properly.
            self.mtilde = 0.0

        # Set initial value for currently-tried rule.
        self.W = -np.inf  # Add a "previous rule value" variable. Init at -inf


        self.K_boundary_probs = 0
            # Number of times need to invoke the boundary condition...

        # Define consumption values with "frozen" params:
        #self.cons = lambda m, EY=EY0, mpc=mpc0, mbar=mbar0: np.maximum(np.minimum(EY + mpc*(m-mbar), m), 0.0)
        self.cons = self.create_consumption_function(EY=EY0, mpc=mpc0, mbar=mbar0)
            # Note: need to time-test the two setups for fastest

        # Record agent values.
        self.y = []
        self.c = []
        self.m = []
        self.a_tm1 = [a0]  # NOTE THAT due to timing, the "a" vector is always "one behind" the rest.
        self.R = []
        self.learning_periods_hist = [] # Record the periods in which learning occurs
        self.EY_hist = []
        self.params = []   # Record the consumption function in use at *end* of a learning period

        # Init container for finding sacrifice values:
        self.sacrifice_vals = []
        self.sacrifice_periods = []

        # Set up checker for whether agent had problems with boundaries
        self.boundry_messages = []

    #def cons2(self, m):
    #    # Replace with compiled version, perhaps. Time differences.
    #    np.maximum(np.minimum(self.EY + self.mpc*(m-self.mbar), m), 0.0)

    def update_policy(self, EY, mpc, mbar):
        self.EY, self.mpc, self.mbar = EY, mpc, mbar
        self.cons = self.create_consumption_function(EY, mpc, mbar)

    def create_consumption_function(self, EY, mpc, mbar):
        '''
        Just to keep the code *mostly* in a single place.
        NOTE: for speed, we *do not* use this for the calculation of the single
        c value in the first few lines of the "step" function. IF YOU CHANGE
        THIS CONSUMPTION FUNCTION YOU MUST CHANGE IT THERE AS WELL.
        '''
        return lambda m, EY=EY, mpc=mpc, mbar=mbar: np.maximum(np.minimum(EY + mpc*(m-mbar), m), 0.0)

    def find_total_experience_policy_sacrifice(self, R, inv_optval, Yshocks, Yprobs, opt_m_shocks, opt_m_probs, grid, tol):
        '''
        First simple approach to examining the total cost of the agent's
        learning experience. Use *all* consumption and state history to
        get a consumption function, then find sacrifice value of this
        consumption function.

        IMPORTANT NOTE: This procedure will only work, assuming a *constant*
        rate of return R, because of the value function solution used.

        Parameters
        ----------
        inv_optval: real-valued function
            Inverse of teh true optimal value function.
        shocks:     np.ndarray
            The discrete shock space for ergodic distribution of states under
            optimal policy.
        probs:      np.ndarray
            Discrete pmf associated with above shocks.
        '''

        # Find the consumption function associated with all consumption and
        # state choices:
        EY = np.mean(self.y)
        mpc, mbar = find_error_minimizing_consumption_ols(c_vals=self.c,
                                                        x_vals=self.m, EY=EY)

        # Create the function and get the sacrifice value:
        #temp_cons = lambda m, EY=EY, mpc=mpc, mbar=mbar: np.maximum(np.minimum(EY + mpc*(m-mbar), m), 0.0)
        temp_cons = self.create_consumption_function(EY=EY, mpc=mpc, mbar=mbar)

        approx_val = find_value_function(c=temp_cons, R=R, beta=self.beta, v0=self.u,
                                         u=self.u, shocks=Yshocks,
                                         probs=Yprobs, m_grid=grid,
                                         eps=tol)
        return calculate_mean_sacrifice_value(vinv=inv_optval,
                                              vhat=approx_val,
                                              xvals=opt_m_shocks,
                                              xprobs=opt_m_probs)

    def step(self, y, R, t):
        '''
        Step forward in time. If current period is in the learning periods, then
        execute a learning step. Otherwise simply record relevant values.

        Parameters
        ----------
        y: float
            Income this period.
        R: float
            Return factor this period.
        t: int
            Current period

        Returns
        ----------
        X: np.ndarray
            Discrete points for discrete probability mass function.

        '''

        # Update all values:
        m = R*self.a_tm1[-1] + y            # Update the state
        c = np.maximum(np.minimum(self.EY + self.mpc*(m-self.mbar), m), 0.0)
            # Update consumption

        # Record values for next period:
        self.a_tm1.append(m-c)
        self.m.append(m)
        self.c.append(c)
        self.y.append(y)
        self.R.append(R)

        # Determine if a learning period; if so, do learning stuff:
        if t in self.learn_periods:
            # Now do learning things!

            # Record this learning period alongside params used self.prev_learn_t:t
            self.learning_periods_hist.append((self.prev_learn_t, t))
            self.params.append({'mpc':self.mpc, 'mbar':self.mbar, 'EY':self.EY})

            # Grab income values to learn from
            t_range = slice(self.prev_learn_t, t)  # "slice" same as array[a:b]
            y_learn = self.y[t_range]  # Grab current learn-y-vals
            m_learn = self.m[t_range]
            R_learn = self.R[t_range]
            c_actual = self.c[t_range]

            # Learn the distribution from experience:
            #print "m_learn =", m_learn
            #print "t =", t
            mids, bins, probs = find_empirical_equiprobable_bins_midpoints(N=self.N, data=np.array(m_learn))

            # Learn the value function from experience:
            vals = single_stream_first_visit_full_value_estimate(cons=self.cons, u=self.u, beta=self.beta,
                                                  Rvec=R_learn, bins=bins, mids=mids, y_shocks=y_learn)


            # Record the "expected value" for the previous rule. NOTE that this
            # is not the value of the *new* rule learned from regret below,
            # but rather the value of the rule we just learned.
            self.W = np.dot(vals, probs)
            # (In soc-learn care, *now* would compare with other learners.)

            # Learn new policy from regret given experience:
            newEY = np.mean(y_learn)
            mpc, mbar, regret_c, regret_x = update_policy_by_regret_ecdf(vals=vals,
                                                bins=bins, mids=mids,
                                                u=self.u, beta=self.beta,
                                                Rvec=R_learn, x_stream=m_learn,
                                                y_stream=y_learn,
                                                EY=newEY)
                # EY=None means to learn the mean from current experience only.
                # EY=np.mean(self.y) means to learn the mean from all income.

            # Check that the mpc and xbar pass quality check. If mpc is below
            # zero, no good; if xbar is below credit limit (default 0), no good.
            # The convext combination acts to not simply get stuck in a poor
            # location.
            if mpc <=0 or mbar <= self.mtilde:
                mpc, mbar = self.convex_combo_parameter_history(mpc, mbar, t)

            # Update the policy function and continue:
            self.update_policy(EY=newEY, mpc=mpc, mbar=mbar)


            if self.record_regret_c_m:
                # Record all the values needed to recreate the learning plots:
                self.regret_c_by_episode.append(regret_c)
                self.actual_c_by_episode.append(c_actual)
                self.actual_m_by_episode.append(regret_x)
                self.actual_y_by_episode.append(y_learn)
                self.actual_m_by_episode_compare.append(m_learn)
                self.m_grid_bins_by_episode.append(bins)
                self.m_grid_mids_by_episode.append(mids)
                self.valsB_by_episode.append(vals)

            # Update the previous learning period...
            self.prev_learn_t = t
    # This should be it for the learning consumer.

    def create_history_of_consumption_functions_plots(self, plot_x_min=0.0,
            plot_x_max=4.0, plot_x_size=100, total_plots=20,
            c_grid_help_plot=[0.0, 0.5, 1.0, 1.5],
            extra_plot_title="", extra_plot_filename="", agent_name="",
            plots_list={'c-vals':True,
                        'y-m-vals':True,
                        'm-grid-probs':True,
                        'y-distrib-vs-y-ecdf':True,
                        'v-functions':True,
                        'v-conditional-probs':True,
                        'c-function-history':True}):
        '''
        plot_x_min: float
            Min of x-axis to plot
        plot_x_max: float
            Max of x-axis to plot.
        plot_x_size: int
            Number of points to plot for things that need smooth plotting.
        total_plots: int
            Total number of episodes to create plots for -- if actual number
            of episodes is less than this, just do all episodes.
        extra_plot_title: string
            Extra bit to add to the begining of the plot titles.
        '''
        if not self.record_regret_c_m:
            print "Cannot create plots unless record_regret_c_m == True"
            return

        if len(plots_list) == 0:
            print "Cannot create plots unless len(plots_list) > 0 and appropriate values are specified."
            return


        # Otherwise, run the loop and create the plots.
        plotgrid = np.linspace(plot_x_min, plot_x_max, plot_x_size)

        c_grid = np.array(c_grid_help_plot) # Freeze this thing...

        # Determine if already found sacrifice values:
        plot_sacrifice_values = len(self.sacrifice_vals) > 0 and  len(self.sacrifice_periods) > 0
        if not plot_sacrifice_values:
            print "Note: Not plotting sacrifice values because len(self.sacrifice_vals) == 0 or  len(self.sacrifice_periods) == 0."

        # Find the "previous sacrifice cons function and sacrifice values"
        previous_consumption_function = self.create_consumption_function(EY=self.params[0]['EY'],
                                                                         mpc=self.params[0]['mpc'],
                                                                         mbar=self.params[0]['mbar'])
        if plot_sacrifice_values:
            prev_sacrifice_value = self.sacrifice_vals[0]

        # Find D:


        # Now start plotting...
        for k in range(1,total_plots):
            '''
            A laundry list of consumption functions to produce:

            * The "income, m-actual, m-speculative, m-grid" plots
            * m-grid and m-grid-probs plots...
            * The "estimated E[v|B] value function, v_true, and v-interpolated-from-experience"
            * The histograms / plots for probabilities for the v-interpolated-from-experience
                * c steady, vary m smoothly
            * True y-income-process-histogram / CDF vs empirical / ECDF
            * The "consumption values, regret-c, previous and current and true consumption function" values
            * The "regular run-through" consumption experience plots, with prev and current cons functions.
            '''

            '''
            Things to work with:

            self.regret_c_by_episode
            self.actual_c_by_episode
            self.actual_m_by_episode
            self.actual_y_by_episode
            self.actual_m_by_episode_compare
            self.m_grid_bins_by_episode
            self.m_grid_mids_by_episode
            self.valsB_by_episode
            '''


            # Set up all values to be used by the plots below:
            t_final = self.D*k
            t_begin = t_final - self.D
            t_values = range(t_begin, t_final)

            y_vals = self.actual_y_by_episode[k]
            m_vals = self.actual_m_by_episode[k]
            c_vals = self.actual_c_by_episode[k]
            c_regret = self.regret_c_by_episode[k]
            m_bins = self.m_grid_bins_by_episode[k]
            m_mids = self.m_grid_mids_by_episode[k]
            EvmB = self.valsB_by_episode[k]

            # income, m-actual, m-speculative plots:
            if plots_list['y-m-vals']:
                # Create these plots. Boom.
                plot_file_name = extra_plot_filename + "y_m_timeseries.pdf"

                # Plot only income values:
                plt.plot(t_values, y_vals)

            pass
            # Plot the consumption functions:
            # prev consumption, current consumption,
            # prev sacrifice value, current sacrifice value

            # Plot the E[v|B] value estimates by itself:

            # Plot the smoothed E[v'|c,m] value estimates, for
            # m-grid values of c:

            # Plot the prob distributions for fixed c-values, and all m-values:

            # Plot the probs associated with each m-bin:

    def run_all_steps(self, y_stream, R_stream, verbose=False, progbar_len=None, total_agents_timing=1):
        '''
        Given an income stream, step until out of time.
        '''

        # Check that values match:
        assert self.T == len(y_stream), 'self.T != len(y_stream)'
        assert self.T == len(R_stream), 'self.T != len(R_stream); self.T = '+str(self.T)+', len(R_stream) = '+str(len(R_stream))

        # Start the progress bar if get the signal to be verbose...
        if verbose:
            print "Starting agent ", self.i
            pbar = ProgressBar(loop_length=self.T, progbar_length=progbar_len, symbol='$')
            t0 = time()

        for t in xrange(self.T):
            self.step(y=y_stream[t], R=R_stream[t], t=t)
            if verbose:
                pbar.update(t)

        if verbose:
            t1 = time()
            pbar.end()
            t_sec = t1-t0
            t_min = t_sec / 60.0
            print "Finishing agent ", self.i
            print "Took "+ str(t_sec) +" seconds, "+ str(t_min) + " minutes for me to finish."
            print "Estimate total time at " + str(t_sec*total_agents_timing) +" seconds, " + str(t_min*total_agents_timing) +" minutes."
            now_start = datetime.now()
            now_finish = now_start + timedelta(seconds=t_sec*total_agents_timing)
            print "Current time is: ", str(now_start)
            print "Projected finish time - if I am first agent - is: ", str(now_finish)
    # This should be it for the learning consumer.

    def convex_combo_parameter_history(self, mpc, mbar, t):
        '''
        If values fall outside a simple no-go zone, take convex combo of
        N_params+1 last param values (if possible) for next trial.
        '''

        if mpc > 0 and mbar > self.mtilde:
            # Then we entered this method when we should not have. Warn user
            # and return the values passed in.
            warnings.warn("WARNING: convex_combo_parameter_history(mpc, mbar) was called incorrectly:  mpc > 0 and mbar > self.mtilde. Returning original values.")
            return mpc, mbar

        # Otherwise create the appropriate convex combo:
        self.K_boundary_probs += 1 # Update this counter.
        if len(self.params) == 1:
            self.boundry_messages.append("On step t="+str(t)+"mpc, mbar= "+str(mpc) +", "+str(mbar)+"; default to previous mpc and xbar.")
            # print "On step t=",t,"mpc, mbar=",mpc,mbar,"; default to previous mpc and xbar.")
            mpc = self.params[-1]['mpc']  # Previous
            mbar = self.params[-1]['mbar']
        elif len(self.params) == 2:
            self.boundry_messages.append("On step t="+str(t)+"mpc, mbar= "+str(mpc) +", "+str(mbar)+"; default to convex combo of 2 prev mpcs, xbars.")
            #print "On step t=",t,"mpc, mbar=",mpc,mbar,"; default to convex combo of 2 prev mpcs, xbars."
            mpc = 0.5*self.params[-1]['mpc'] + 0.5*self.params[-2]['mpc']  # Convex combo of last two
            mbar = 0.5*self.params[-1]['mbar'] + 0.5*self.params[-2]['mbar']
        else:
            self.boundry_messages.append("On step t="+str(t)+"mpc, mbar= "+str(mpc) +", "+str(mbar)+"; default to convex combo of 3 prev mpcs, xbars.")
            #print "On step t=",t,"mpc, mbar=",mpc,mbar,"; default to convex combo of 3 prev mpcs, xbars."
            # Convex combo of last three:
            mpc = (1.0/3*self.params[-1]['mpc'] + 1.0/3*self.params[-2]['mpc'] +
                (1.0-(1.0/3+ 1.0/3))*self.params[-3]['mpc'])
            mbar = (1.0/3*self.params[-1]['mbar'] + 1.0/3*self.params[-2]['mbar'] +
                  (1.0-(1.0/3+ 1.0/3))*self.params[-3]['mbar'])

        # Return the convex-combo-ed mpc and mbar
        return mpc, mbar


    def find_all_sacrifice_values(self, Ypoints, Yprobs, grid, tol, inv_optval, sim_m_mids, sim_m_probs, verbose, progbar_len=None, total_agents_timing=1):
        '''
        This is a simple function to find all own sacrifice values. Note that
        this will delete and overwrite any sacrifice values already found.

        Now individual values will be stored at the agent level.
        '''

        # Zero out the sacrifice values and periods lists
        self.sacrifice_vals = []
        self.sacrifice_periods = []

        # Check whether R-values differ over time. If so, need to solve
        # differently: either deterministic R-change, or R also in expectation.
        assert np.sum(np.diff(self.R)) == 0, "R values differ over time! Will need to solve appropriate value/policy iteration problem; current assumes constant R."

        if verbose:
            print "Starting agent", self.i, "finding all sacrifice values"
            pbar = ProgressBar(loop_length=len(self.params), progbar_length=progbar_len, symbol='#')
            t0 = time()

        s = 0 # ctr for pbar
        for cons_param, learn_t in zip(self.params, self.learning_periods_hist):
            # mpc, mbar,
            temp_cons = lambda m, EY=cons_param['EY'], mpc=cons_param['mpc'], mbar=cons_param['mbar']: np.maximum(np.minimum(EY + mpc*(m-mbar), m), 0.0)

            approx_val = find_value_function(c=temp_cons, R=self.R[0], beta=self.beta, v0=self.u,
                                             u=self.u, shocks=Ypoints,
                                             probs=Yprobs, m_grid=grid,
                                             eps=tol, maxiter=5000, verbose=False)
            temp_sacrifice_val = calculate_mean_sacrifice_value(vinv=inv_optval,
                                                                 vhat=approx_val,
                                                                 xvals=sim_m_mids,
                                                                 xprobs=sim_m_probs)
            self.sacrifice_vals.append(temp_sacrifice_val)
            self.sacrifice_periods.append(learn_t)

            if verbose:
                pbar.update(s)
            s+=1

        # That should do it. Agents now all have their sacrifice values saved.

        if verbose:
            t1 = time()
            pbar.end()
            t_sec = t1-t0
            t_min = t_sec / 60.0
            print "Finishing agent", self.i, "finding all sacrifice values"
            print "Took "+ str(t_sec) +" seconds, "+ str(t_min) + " minutes for me to finish."
            print "Estimate total time at " + str(t_sec*total_agents_timing) +" seconds, " + str(t_min*total_agents_timing) +" minutes."
            now_start = datetime.now()
            now_finish = now_start + timedelta(seconds=t_sec*total_agents_timing)
            print "Current time is: ", str(now_start)
            print "Projected finish time - if I am first agent - is: ", str(now_finish)



# Define simple functions the code can use to
# force multiprocessing
def agent_run_all_steps(obj, dict_inputs):
    obj.run_all_steps(**dict_inputs)
    return obj

def agent_find_all_sacrifice_values(obj, dict_inputs):
    obj.find_all_sacrifice_values(**dict_inputs)
    return obj

# Create the learning consumer simulation:
class LearningSimulation(object):

    def __init__(self, a0, u, f, beta, R, income_generator, Yprobs, Ypoints,
    T_ergodic_periods, N_ergodic_agents, T_autolearn, N_multiagent, D_autolearn,
    Nbins_for_regret_learning, grid, tol, discarded_periods, progbar_len,
    sim_verbose, verbose, sacrifice_quantiles_q, Nbins_for_sacrifice_approx=501,
    m0=None,
    mpc0=None, mbar0=None,
    ncores=None, fraction_cores_to_use=0.75,
    record_regret_c_m=False):
        '''

        '''
        self.a0 = a0
        self.u, self.f, self.beta, self.R = u, f, beta, R
        # NOTE: need to remove self.f and test that nothing changes.
        self.T_autolearn, self.N_multiagent = T_autolearn, N_multiagent
        self.progbar_len = progbar_len

        self.D_autolearn = D_autolearn
        self.Nbins_for_regret_learning = Nbins_for_regret_learning

        self.Ypoints = Ypoints
        self.Yprobs = Yprobs
        self.grid = grid
        self.tol = tol

        self.verbose = sim_verbose

        # Specifically record the regret consumption values along with the
        # cash-on-hand values, which are used to find the next cons values
        self.record_regret_c_m = record_regret_c_m

        # Set up cores:
        number_cores_available = multiprocessing.cpu_count()
        self.fraction_cores_to_use = fraction_cores_to_use

        # Work through possible cases:
        if ncores is None:
            # Then need to set it equal to value implied by fraction_cores_to_use
            raw_cores_to_use = int(number_cores_available*self.fraction_cores_to_use)

            # Make sure this is bound between [1, number_cores_available] :
            self.ncores = max(1, min(raw_cores_to_use, number_cores_available))

        elif ncores > number_cores_available:
            # If not None, but greater than physically available, assume want to
            # grab all available:
            self.ncores = number_cores_available
        else:
            # Finally just set the ncores:
            self.ncores = ncores

        # Init container for sacrifice values.
        self.all_sacrifice_vals = []
        self.all_sacrifice_periods = []
        self.all_param_vals = []
        self.all_sacrifice_Nobs = {}
        self.all_sacrifice_quantiles_x = []
        self.all_sacrifice_quantiles_q = sacrifice_quantiles_q
        self.all_sacrifice_ecdf = {}
        self.sacrifice_vals_by_periods = {}

        # Solve the true opimization problem and use simulation to find the
        # ergodic distribution of states under the true choice
        if self.verbose:
            print "Solving for true optimal policy..."
        t0=time()
        # First find the true optimal consumption function:
        self.optcons, self.optval = generalized_policy_iteration(u=u, f=f,
                                                       beta=beta, grid=grid,
                                                       tol=tol, Ypoints=Ypoints,
                                                       Yprobs=Yprobs, v=None,
                                                       policy=None, verbose=verbose)

        # Create a linear inverse of the optimal value function:
        # NOTE: need to calculate inverse value function on large grid!
        self.inv_optval = InterpolatedUnivariateSpline(x=self.optval.get_coeffs(), y=self.optval.get_knots(), k=1)

        if self.verbose:
            print "Simulating optimal behavior for ergodic distrib of m..."

        t1=time()

        # Simulate consumption rule and find 5%, 95% conf intervals
        sim_m, sim_c, y_shocks, mvals = generate_simulated_choice_data_continuous(cons=self.optcons,
                                                      income_generator=income_generator,
                                                      periods=T_ergodic_periods, Nagents=N_ergodic_agents, R=R,
                                                      discard_periods=discarded_periods, m0=None)

        # Create a flattened version of m-values, for many future uses:
        flat_sim_m = sim_m.ravel()
        t2=time()

        if self.verbose:
            print "Find the discrete distribution of ergodic optimal m-choices"

        # Find a discrete approximation to the the distribution of m-values. This
        # will be used to calculate the sacrifice value for a particular approximate
        # policy.
        Nbins_for_sacrifice_approx = min(Nbins_for_sacrifice_approx, len(flat_sim_m)/2.0)
        self.sim_m_mids, self.sim_m_bins, self.sim_m_probs = find_empirical_equiprobable_bins_midpoints(N=Nbins_for_sacrifice_approx, data=flat_sim_m)
        t3=time()
        if self.verbose:
            print "...took",t3-t2, "sec"

        # Now generate an income stream for one agent and have that agent learn.
        # When the agent is done learning determine the sacrifice value of the rule(s)
        # experienced.

        if self.verbose:
            print "Create agent..."

        # Set up the income stream:
        self.income_stream = income_generator((T_autolearn, N_multiagent))


        # Create a consumer agents:
        self.agents = []
        for i in range(N_multiagent):
            a0_temp = a0[i]
            EY_temp = np.mean(self.income_stream[:,i])
            if mpc0 is None:
                mpc0=1.0
            if mbar0 is None:
                mbar0=EY_temp
            self.agents.append(RegretAgent(u=u, beta=beta, a0=a0_temp,
                                  D=D_autolearn, T=T_autolearn,
                                  EY0=EY_temp,
                                  mpc0=mpc0, mbar0=mbar0,
                                  N=Nbins_for_regret_learning,
                                  i=i,
                                  record_regret_c_m=self.record_regret_c_m))
        t4=time()
        if self.verbose:
            print "Took", t4-t0, "sec to set up simulation."

    """
    # Note: staticmethod doesn't work for pickling.
    @staticmethod
    def agent_run_all_steps(obj, dict_inputs):
        '''
        Define a static method we can use in the parallel mapping in
        run_simulation.
        '''
        obj.run_all_steps(**dict_inputs)
        return obj
    """


    def run_simulation(self):

        # Test whether values are correct:
        assert self.N_multiagent == len(self.agents), "self.N_multiagent != len(self.agents). self.N_multiagent "+str(self.N_multiagent)+', len(self.agents) = '+ str(len(self.agents))
        assert self.T_autolearn == self.income_stream.shape[0], "self.T_autolearn != self.income_stream.shape[0]. self.T_autolearn "+str(self.T_autolearn)+', self.income_stream.shape[0] = '+ str(self.income_stream.shape[0])

        # Determine number of cores we can use:
        num_jobs = self.ncores
        if self.verbose:
            print "num_jobs =", num_jobs

        # Calculate the number of agents per core:
        agents_per_core = np.ceil(self.N_multiagent/float(num_jobs))
            # Any "mod" agents left over will presumably be thrown onto the last
            # core.


        # Create the list of values that are handed to each agent.
        all_agent_inputs = []
        for i in range(len(self.agents)):
            all_agent_inputs.append({'y_stream':self.income_stream[:,i],
                                     'R_stream':np.repeat(self.R, self.T_autolearn),
                                     'verbose':False,
                                     'progbar_len':None,
                                     'total_agents_timing':agents_per_core})
                                     # Note:

        # Set first and last agent to potentially be 'verbose':
        all_agent_inputs[0]['verbose'] = self.verbose
        all_agent_inputs[0]['progbar_len'] = self.progbar_len
        all_agent_inputs[-1]['verbose'] = self.verbose
        all_agent_inputs[-1]['progbar_len'] = self.progbar_len
            # The purpose of this is to get signals sent to the user at both the
            # start and end of the sim. ... will see if it works...

        # Now call parallel call to send off all agents to different cores...
        t0 = time()
        results =  Parallel(n_jobs=num_jobs)(delayed(agent_run_all_steps)(*args) for args in zip(self.agents, all_agent_inputs))
        t1 = time()
        # Now save the output:
        self.agents = None  # Make sure we cut ties with old list.
        self.agents = sorted(results)

        t2=time()
        if self.verbose:

            t_sec0 = t1-t0
            t_min0 = t_sec0 / 60.0

            t_sec1 = t2-t1  # No need to calculate minutes here...not currently at least

            print "Took", t_sec0, "sec,", t_min0, "min to run", self.N_multiagent, "- agent sim for", self.T_autolearn, "periods."
            print "Took", t_sec1, "sec to sort ", self.N_multiagent, " agents."


    def find_sacrifice_values(self):
        '''
        Parallelized version of finding agent sacrifice values. This is a very
        simple function which simply asks all agents, in parallel, to find and
        record the sacrifice values for their history of consumption functions.
        Agents are then all are sorted as before, which guarantees that agents
        (and results) are in the correct order.
        '''

        # Recall:
        # find_all_sacrifice_values(self, Ypoints, Yprobs, grid, tol, inv_optval, approx_val, sim_m_mids, sim_m_probs)

        if self.verbose:
            print "Finding all sacrifice values."

        # Test whether values are correct:
        assert self.N_multiagent == len(self.agents), "self.N_multiagent != len(self.agents). self.N_multiagent "+str(self.N_multiagent)+', len(self.agents) = '+ str(len(self.agents))

        # Determine number of cores we can use:
        num_jobs = self.ncores
        if self.verbose:
            print "num_jobs =", num_jobs

        # Calculate the number of agents per core:
        agents_per_core = np.ceil(self.N_multiagent/float(num_jobs))
            # Any "mod" agents left over will presumably be thrown onto the last
            # core.

        # Create the list of values that are handed to each agent.
        all_agent_inputs = []
        for i in range(len(self.agents)):
            all_agent_inputs.append({'Ypoints':self.Ypoints,
                                     'Yprobs':self.Yprobs,
                                     'grid':self.grid,
                                     'tol':self.tol,
                                     'inv_optval':self.inv_optval,
                                     'sim_m_mids':self.sim_m_mids,
                                     'sim_m_probs':self.sim_m_probs,
                                     'verbose':False,
                                     'progbar_len':self.progbar_len,
                                     'total_agents_timing':agents_per_core})

        # Set first and last agent to potentially be 'verbose':
        all_agent_inputs[0]['verbose'] = self.verbose
        all_agent_inputs[0]['progbar_len'] = self.progbar_len
        all_agent_inputs[-1]['verbose'] = self.verbose
        all_agent_inputs[-1]['progbar_len'] = self.progbar_len
            # The purpose of this is to get signals sent to the user at both the
            # start and end of the sim. ... will see if it works...

        # Now tell all agents to calculate their sacrifice values:
        t0 = time()
        results =  Parallel(n_jobs=num_jobs)(delayed(agent_find_all_sacrifice_values)(*args) for args in zip(self.agents, all_agent_inputs))
        t1 = time()
        # Now save the output:
        self.agents = None  # Make sure we cut ties with old list.
        self.agents = sorted(results)

        t2=time()
        if self.verbose:
            print "Took", t1-t0, "sec, ", (t1-t0)/60.0 ,"min to run", self.N_multiagent, "- agent sim for", self.T_autolearn, "periods."
            print "Took", t2-t1, "sec to sort ", self.N_multiagent, " agents."




    def run_simulation2(self):
        '''
        Non-parallel version.
        '''

        t0 = time()
        if self.verbose:
            pbar = ProgressBar(loop_length=self.T_autolearn, progbar_length=self.progbar_len, symbol='$')

        # Want to create the list of values that are handed to each agent.
        assert self.N_multiagent == len(self.agents), "self.N_multiagent != len(self.agents)"

        for t in xrange(self.T_autolearn):
            y_cross = self.income_stream[t,:]
            for i, learner in enumerate(self.agents):
                learner.step(y=y_cross[i], R=self.R, t=t)
            if self.verbose:
                pbar.update(t)

        if self.verbose:
            pbar.end()
        t1=time()
        if self.verbose:
            print "Took", t1-t0, "sec,", (t1-t0)/60.0 ,"min to run", self.N_multiagent, "- agent sim for", self.T_autolearn, "periods."


    def find_sacrifice_values2(self):
        '''
        Older non-parallel version.
        Presumably after the simulation is finished, find the sacrifice values
        associated with the agents -- at each learning period (when rules
        change), as well as the "total sacrifice value" associated with the
        rule which best fits the entire agents' experience.

        KEEP IMPORTANT NOTE: the consumption function in the lines
                        temp_cons = lambda m, EY=cons_param['EY'], mpc=cons_param['mpc'], mbar=cons_param['mbar']: np.maximum(np.minimum(EY + mpc*(m-mbar), m), 0.0)
        MUST MATCH THE VALUE FOR THE CONSUMER AGENTS.

        Much of this should likely be moved directly to the consumer agent
        themselves. BUT, don't do that until actually get around to changing the
        consumption functions in any significant way (eg. mtilde)
        '''


        if self.verbose:
            print "Done with the learning loop! Now find all sacrifice values."
            pbar = ProgressBar(loop_length=self.N_multiagent, progbar_length=self.progbar_len, symbol='$')

        t0 = time()
        # After loop finished, find sacrifice value of
        self.all_sacrifice_vals = []
        self.all_sacrifice_periods = []
        self.all_param_vals = []
        self.all_sacrifice_Nobs = {}
        self.all_sacrifice_quantiles_x = {}
        self.all_sacrifice_ecdf = {}
        self.sacrifice_vals_by_periods = {}
        self.all_total_experience_sacrifice_values = []
        i=0
        for learner in self.agents:

            # Find sacrifice value of entire experience.
            tempsv = learner.find_total_experience_policy_sacrifice(R=self.R,
                                        inv_optval=self.inv_optval,
                                        Yshocks=self.Ypoints,
                                        Yprobs=self.Yprobs,
                                        opt_m_shocks=self.sim_m_mids,
                                        opt_m_probs=self.sim_m_probs,
                                        grid=self.grid,
                                        tol=self.tol)

            # Save...
            self.all_total_experience_sacrifice_values.append(tempsv)

            sacrifice_vals = []
            sarcifice_periods = []
            param_vals = []
            for cons_param, learn_t in zip(learner.params, learner.learning_periods_hist):
                # mpc, mbar,
                temp_cons = lambda m, EY=cons_param['EY'], mpc=cons_param['mpc'], mbar=cons_param['mbar']: np.maximum(np.minimum(EY + mpc*(m-mbar), m), 0.0)

                approx_val = find_value_function(c=temp_cons, R=self.R, beta=self.beta, v0=self.u,
                                                 u=self.u, shocks=self.Ypoints,
                                                 probs=self.Yprobs, m_grid=self.grid,
                                                 eps=self.tol, maxiter=5000, verbose=False)
                temp_sacrifice_val = calculate_mean_sacrifice_value(vinv=self.inv_optval,
                                                                     vhat=approx_val,
                                                                     xvals=self.sim_m_mids,
                                                                     xprobs=self.sim_m_probs)
                sacrifice_vals.append(temp_sacrifice_val)
                sarcifice_periods.append(learn_t)
                param_vals.append(deepcopy(cons_param))

                # Save vals so we can look at ECDF over time:
                if self.sacrifice_vals_by_periods.has_key(learn_t):
                    self.sacrifice_vals_by_periods[learn_t].append(temp_sacrifice_val)
                else:
                    self.sacrifice_vals_by_periods[learn_t] = [temp_sacrifice_val]

            # Gather:
            self.all_sacrifice_vals.append(sacrifice_vals)
            self.all_sacrifice_periods.append(sarcifice_periods)
            self.all_param_vals.append(param_vals)
            if self.verbose:
                pbar.update(i)
            i+=1

        if self.verbose:
            pbar.end()

        t1 = time()
        if self.verbose:
            print "Took", t1-t0, "sec to find sacrifice values for ", self.N_multiagent, "agents for", len(cons_param), "periods."


# Create "raw" consumer-learning code here:
if __name__ == "__main__":
    import setup_parameters_functions_regret_learning_computational_letter as param
    from simulation_library import generate_simulated_choice_data_continuous
    from optimization_library import generalized_policy_iteration, find_value_function
    from HACKUtilities import calculate_mean_sacrifice_value
    from progress_bar import ProgressBar
    from copy import deepcopy
    from time import time
    import pylab as plt
    import dill as pickle
    from datetime import datetime

    # Set up values to try:
    N_list = np.array([3, 5, 9, 13, 25, 33, 65, 95])
    D_list = np.array([5,13,33,95,201,501])

    # Set up values to try:
    #D_list = [5]
    #N_list = [95]

    N_list = np.array([5, 13, 33, 95])
    D_list = np.array([13,95,301])

    D_list = [25]
    N_list = [7]

    inval = None
    inval = raw_input("Please enter the numerical option below:\n\t1\tfor regular run\n\t2\tfor full-history run\n\t3\tfor full-history and policy projection run\nSelection: ")

    if int(inval) == 1:

    #D_list = [7, 17]
    #N_list = [5]

        print "Simulation set started at local time: ", datetime.now()

        sim_results = []
        for n in N_list:
            for d in D_list:
                if n <= d:
                    # Create a simulation, run it, set up the results, and save them:
                    param.set_up_random_variables()
                    temp_sim = LearningSimulation(a0=param.a0,
                                                  u=param.u,
                                                  f=param.f,
                                                  beta=param.beta,
                                                  R=param.R,
                                                  income_generator=param.income_generator,
                                                  Yprobs=param.Yprobs,
                                                  Ypoints=param.Ypoints,
                                                  T_ergodic_periods=param.totalT,
                                                  N_ergodic_agents=param.Nagents,
                                                  T_autolearn=param.T_autolearn,
                                                  sim_verbose=param.sim_verbose,
                                                  Nbins_for_regret_learning=n,
                                                  grid=param.grid,
                                                  tol=param.tol,
                                                  discarded_periods=param.discarded_periods,
                                                  N_multiagent=param.N_multiagent,
                                                  D_autolearn=d,
                                                  sacrifice_quantiles_q=param.sacrifice_quantiles_q,
                                                  progbar_len=param.progbar_len,
                                                  verbose=param.verbose,
                                                  Nbins_for_sacrifice_approx=param.Nbins_for_sacrifice_approx,
                                                  m0=None)
                    temp_sim.run_simulation()
                    temp_sim.find_sacrifice_values()

                    # Save the simulation:
                    sim_results.append(temp_sim)

                    # Detach the simulation variable, just to be safe:
                    temp_sim = None


        print "Simulation set finished at local time: ", datetime.now()
        # Need to save these physically:
        with open('simulation_results_regular.pickle', 'wb') as handle:
            pickle.dump(sim_results, handle)

    elif int(inval) == 2:

        print "Simulation set started at local time: ", datetime.now()

        sim_results = []
        for n in N_list:
            for d in D_list:
                if n <= d:
                    # Create a simulation, run it, set up the results, and save them:
                    param.set_up_random_variables()
                    temp_sim = LearningSimulation_OptimisticPI(a0=param.a0,
                                                  u=param.u,
                                                  f=param.f,
                                                  beta=param.beta,
                                                  R=param.R,
                                                  income_generator=param.income_generator,
                                                  Yprobs=param.Yprobs,
                                                  Ypoints=param.Ypoints,
                                                  T_ergodic_periods=param.totalT,
                                                  N_ergodic_agents=param.Nagents,
                                                  T_autolearn=param.T_autolearn,
                                                  sim_verbose=param.sim_verbose,
                                                  Nbins_for_regret_learning=n,
                                                  grid=param.grid,
                                                  tol=param.tol,
                                                  discarded_periods=param.discarded_periods,
                                                  N_multiagent=param.N_multiagent,
                                                  D_autolearn=d,
                                                  sacrifice_quantiles_q=param.sacrifice_quantiles_q,
                                                  progbar_len=param.progbar_len,
                                                  verbose=param.verbose,
                                                  Nbins_for_sacrifice_approx=param.Nbins_for_sacrifice_approx,
                                                  m0=None)
                    temp_sim.run_simulation()
                    temp_sim.find_sacrifice_values()

                    # Save the simulation:
                    sim_results.append(temp_sim)

                    # Detach the simulation variable, just to be safe:
                    temp_sim = None


        print "Simulation set finished at local time: ", datetime.now()

        # Need to save these physically:
        with open('simulation_results_OPI_learers.pickle', 'wb') as handle:
            pickle.dump(sim_results, handle)

    else:
        print "Please retry and enter a correct value."
