'''
setup_parameters_functions_for_regret_learning_computational_letter.py

This parameter setup file sets up the parameters and the functions associated
with the computational letter "Overview of Individual Regret Learning." This
letter will summarize the regret-learning results for internal audiences.

By Nathan M. Palmer
On 24 Feb 2015
'''

# Import necessary files:
from __future__ import division
import numpy as np
import pylab as plt
from copy import deepcopy
import scipy.stats as stats
from MicroDSOPs_library import calculate_mean_one_lognormal_discrete_approx

# ------------------------------------------------------------------------------
# --------------------- Set up problem values ----------------------------------
# ------------------------------------------------------------------------------
rho = 3.0
R   = 1.03
beta = 0.95

# Confirm: the following is for finding the ergodic distribution...
Nagents = 5000  # commented out are used in grid code, use again for grid!
totalT = 1500  #NOTE: *not* the total learning time. That is T_autolearn

Nbins_for_regret_learning = 7  # Should be set by grid code for grid runs

all_sequential_plots = False  # Should we show all plots in the total sequence?

## NOTE: THIS SETUP GIVES  US THE WEIRD TROUBLE AT HERE: Sacrifice value for d = 125 = 0.133359857308
## See block string/comment below

D_autolearn = 25    # TODO: NOTE: THis is currently set by the simulation grid code.
                    # Interval for learning from own experience
T_autolearn =  801 #19000 #4100  # 800 #Upper limit on how many learning sessions we will observe.
N_multiagent = 3   # number of agents to be used for multi-agent sim


EY_equals_mean = 1  # 0=true mean, 1=y_hist, 2=y_temp_stream

use_one_interval_y_experience = True    # Use this as a "consistency check" on all other values.
                                        #

auto_mids_bins = True  # Should agent get bins, mids only from own experience?
auto_mids_bins_full_yhist = False       # If agents use bins/mids from own
                                        # experience, should use full yhist?
use_full_yhist_for_value_learn = False   # Should agents use accumulated income to learn value?

agents_sophisticated_regret = False # Should agents re-calculate all x-history
                                    # when doing "regret" values?
relative_value = False # Need to fix before can use

use_ecdf_to_get_vprime = True       # Should agents use nearest-neighbor to get
                                    # E[V'], or use ecdf from experience to
                                    # "properly" calculate?
use_full_full_yhist_in_ecdf_vprime = False   # If "properly" calculating with
                                            # ECDF, should use full yhist?
use_ols_minimization = True         # Should we use OLS or numerical minimization?


trim_x = False
trim_pctles = (2.,98.)


sim_verbose = True   # Should the multiagent simulation be verobse?

sacrifice_quantiles_q = np.array([0.1, 0.2, 0.25, 0.3, 0.3333, 0.4, 0.5, 0.6, 0.6667, 0.7, 0.75, 0.8, 0.9])

# Parameter for own-experience learning:
# D = [6, 8, 10, 20, 24, 26, 30, 50]

u = lambda c, rho=rho: c**(1.0-rho) / (1.0-rho) # Utility function
u.prime = lambda c, rho=rho: c**(-rho)          # Add derivative attribute/method
u.prime_inv = lambda w, rho=rho: w**(-1.0/rho)          # Add derivative attribute/method


# Construct the flattened state-space and pmf:
N = 15
sigma = 0.2
mu = -0.5*(sigma**2)
shocks, probs = calculate_mean_one_lognormal_discrete_approx(N, sigma)

distrib = stats.lognorm(sigma, 0, np.exp(mu))


# ------ Set up discrete approx ------
pdf = distrib.pdf

Ypoints= deepcopy(shocks)
Yprobs = deepcopy(probs)

simulation_seed = 1234567890    # nice picture seed, second time run: 1237890456, 1023456789
                                # Strange path: 1023674589
                                # Other: 987654321, 1234567890
max_seed_int = (2**32)-1

def set_up_random_variables(newseed=None):

    # One of the very few places that I consider this acceptable reason to
    # create a global var:
    global RNG
    global setup_RNG
    if newseed is None:
        RNG = np.random.RandomState(simulation_seed)            # Can be used generally
        setup_RNG = np.random.RandomState(simulation_seed-1)    # Used for setup of params
    else:
        RNG = np.random.RandomState(newseed)            # Can be used generally
        setup_RNG = np.random.RandomState(newseed-1)    # Used for setup of params

    # Generate a new seed for multiagents from this sim seed. Use this to create the
    # multi-agent random number generator. Note that we must use an intenger between
    # 0 and 2**31-1, because of the limitation on 32-bit seeds for RandomState.
    global multiagent_seed
    global multiagent_RNG

    multiagent_seed = setup_RNG.randint(low=0,high=max_seed_int)  # Grab a single random int as seed.
    multiagent_RNG = np.random.RandomState(multiagent_seed)

    # Now create the income generator which
    global income_generator_seed
    global income_generator_RNG
    global income_generator

    income_generator_seed = setup_RNG.randint(low=0,high=max_seed_int)
    income_generator_RNG = np.random.RandomState(income_generator_seed)
    income_generator = lambda n, mu=mu, sig=sigma, RNG=income_generator_RNG: RNG.lognormal(mean=mu, sigma=sig, size=n)


# Set up all random variables. Note that we set this up so that we can re-run
# the exact same simulation:
set_up_random_variables()


a0 = income_generator(N_multiagent)
Nbins_for_sacrifice_approx = 501


# Set up the grid for policy and value function interpolation:
grid_min = 1e-3
grid_max = 5
grid_size = 30
plotgrid_size = 100


grid = np.linspace(grid_min, grid_max, grid_size)
plotgrid = np.linspace(grid_min, grid_max, plotgrid_size)

# Set up the transition function:
f  = lambda x, c, z, R=R:  R*(x-c) + z  # Given 'pre-decision' (regular) state
fa = lambda a, z, R=R: R*a + z          # Given 'post-decision' state

tol = 1e-6

discarded_periods = 20

verbose=False

m0 = np.percentile(grid, 25)  # Set the m0 value as 25th percentile of grid

example_single_long_income_T = 10000
example_single_long_income_seed = simulation_seed + 1

N_show_single_stream_value_estimates = 100

progbar_len = None  # Will use default length

N_friends_for_ave_demo = 200 #2000   #800
T_periods_for_ave_demo = 500 #10000   #1000
#
