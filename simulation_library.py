'''
simulation_library.py

By Nathan M. Palmer
Updates: 12 Mar 2015
'''
from __future__ import division
import warnings
import numpy as np
import scipy.stats as stats
from statsmodels.tools.tools import ECDF
from scipy.stats.mstats import mquantiles


# First, a "monkeypatch" to warnings, to print pretty-looking warnings. The
# default behavior of the "warnings" module is to print some extra, silly-
# looking things when the user calls a warning. The official "fix" for this is
# apparently to "monkeypatch" the warnings module. See the discussion here, and
# the included links:
# http://stackoverflow.com/questions/2187269/python-print-only-the-message-on-warnings
# I implement this fix directly below, for all simulation and solution
# utilities.
def _warning(
    message,
    category = UserWarning,
    filename = '',
    lineno = -1):
    print(message)
warnings.showwarning = _warning


def which_bin(m, bin_cutoffs):
    '''
    Given ordered cutoffs for a parition of the 1D space M, determine the index
    of the appropriate "bins" for the individual state value m.

    By: Nathan M. Palmer
    Latest: 24 Feb 2015
    '''

    if  np.isinf(bin_cutoffs[0]) or np.isinf(bin_cutoffs[-1]):
        print "WARNING: inf values in bin_cutoffs[0] or bin_cutoffs[-1]:  bin_cutoffs[0] = " + str(bin_cutoffs[0]), "bin_cutoffs[-1] = " + str(bin_cutoffs[-1])

    return np.searchsorted(bin_cutoffs, m)


# TODO: need to add a component which will *try to find the steady state distrib
# from the conditional-pmf-from-consumption-function, *USING THE MIDS AND BINS
# FROM PROPER DISCRETIZING.*

def find_stationary_discrete_distrib(cons, R, bins, mids, probs, data):

    # Find transition matrix:
    dist_tomorrow = []
    for x0 in mids:
        dist_next_period = find_conditional_pmf_from_cons(x0, cons(x0), R, bins, data)
        dist_tomorrow.append(dist_next_period)
    trans_matrix = np.array(dist_tomorrow)  # TODO: confirm correct dimensions

    # Find ergodic/stationary distribution:

    pass

def find_conditional_pmf(x0, c0, R, bins, data):
    '''
    Given initial state x0, consumption associated with that state c0,
    use, R, bins, data to determine the implied pmf distribution over
    tomorrow's state variable.

    ASSUME that bins *excludes* the -np.inf and np.inf on either side.
    That is, assume that len(bins) = Nbins-1. That is, the number of *midpoints*
    for our pmf is Nbins = len(bins)+1.

    Confusion from the terminology -- "bins" really should be "cutoffs." Will
    refactor and correct. TODO.

    Recall: law of motion looks like:

        x' = R(x-c(x)) + y

    We have data on y but not the true distribution. We want the following:
    Given CDF/ECDF:
        ^       ,-----
        |     /
        |    /
        |   /
        +--`|--|----------->
           h0  h1
    We want P(h0 <= x' < h1)
        =  Fx(h1)-Fx(h0)
        -> Fx(h1) = Fx(x' <= h1) = Fx(R(x-c(x)) + y <= h1) = Fy(y <= h1 - R(x-c(x)))

    Let zi = hi - R(x-c(x)), ECDF = Fy from the data as an empirical CDF. Now,

        P(h0 <= x' < h1) = Fx(h1)-Fx(h0) = Fy(z1) - Fy(z0) ~= ECDF(z1) - ECDF(z0).

    We do this for all cutoffs indicated by "bins."

    Return the implied probabilities.
    '''

    cutoffs = np.array(bins)

    # Ensure that we have +/- inf book-ending the cutoffs; useful for the loop.
    if not np.isinf(cutoffs[0]):
        cutoffs = np.append(-np.inf, cutoffs)
    if not np.isinf(cutoffs[-1]):
        cutoffs = np.append(cutoffs, np.inf)

    # Set the ecdf function:
    ecdf = ECDF(data)   # TODO: Set appropriate ECDF. Review the mquantiles
                        # function as used below

    pmf = np.ones(len(cutoffs)-1) + np.nan
    a = x0-c0
    b = R*a
    i=0
    for h0, h1 in zip(cutoffs[:-1], cutoffs[1:]):
        z0 = h0 - b
        z1 = h1 - b
        pmf[i] = ecdf(z1) - ecdf(z0)
        i+=1

    if np.any(pmf < 0):
        print "WARNING: np.any(pmf < 0). pmf < 0:", pmf < 0
    abs_0 = np.abs(np.sum(pmf) - 1.0)
    if not np.isclose(abs_0, 0.0):
        print "WARNING: np.sum(pmf) != 1. np.sum(pmf):", np.sum(pmf)

    return pmf



def find_conditional_pmf_from_cons(x0, cons, R, bins, data):
    '''
    Given initial state x0, use consumption function, R, bins, data to determine
    the implied pmf distribution over tomorrow's state variable.

    ASSUME that bins *excludes* the -np.inf and np.inf on either side.
    That is, assume that len(bins) = Nbins-1. That is, the number of *midpoints*
    for our pmf is Nbins = len(bins)+1.

    Confusion from the terminology -- "bins" really should be "cutoffs." Will
    refactor and correct. TODO.

    Recall: law of motion looks like:

        x' = R(x-c(x)) + y

    We have data on y but not the true distribution. We want the following:
    Given CDF/ECDF:
        ^       ,-----
        |     /
        |    /
        |   /
        +--`|--|----------->
           h0  h1
    We want P(h0 <= x' < h1)
        =  Fx(h1)-Fx(h0)
        -> Fx(h1) = Fx(x' <= h1) = Fx(R(x-c(x)) + y <= h1) = Fy(y <= h1 - R(x-c(x)))

    Let zi = hi - R(x-c(x)), ECDF = Fy from the data as an empirical CDF. Now,

        P(h0 <= x' < h1) = Fx(h1)-Fx(h0) = Fy(z1) - Fy(z0) ~= ECDF(z1) - ECDF(z0).

    We do this for all cutoffs indicated by "bins."

    Return the implied probabilities.
    '''
    cutoffs = np.array(bins)

    # Ensure that we have +/- inf book-ending the cutoffs; useful for the loop.
    if not np.isinf(cutoffs[0]):
        cutoffs = np.append(-np.inf, cutoffs)
    if not np.isinf(cutoffs[-1]):
        cutoffs = np.append(cutoffs, np.inf)

    # Set the ecdf function:
    ecdf = ECDF(data)
    pmf = np.ones(len(cutoffs)-1) + np.nan
    a = x0-cons(x0)
    b = R*a
    i=0
    for h0, h1 in zip(cutoffs[:-1], cutoffs[1:]):
        z0 = h0 - b
        z1 = h1 - b
        pmf[i] = ecdf(z1) - ecdf(z0)
        i+=1

    if np.any(pmf < 0):
        print "WARNING: np.any(pmf < 0). pmf < 0:", pmf < 0
    if np.sum(pmf) > 1:
        print "WARNING: np.sum(pmf) > 1. np.sum(pmf):", np.sum(pmf)

    return pmf




def find_conditional_pmf_from_cdf(x0, c0, R, bins, cdf):
    '''
    Given initial state x0, consumption associated with that state c0,
    use, R, bins, data to determine the implied pmf distribution over
    tomorrow's state variable.

    ASSUME that bins *excludes* the -np.inf and np.inf on either side.
    That is, assume that len(bins) = Nbins-1. That is, the number of *midpoints*
    for our pmf is Nbins = len(bins)+1.

    Confusion from the terminology -- "bins" really should be "cutoffs." Will
    refactor and correct. TODO.

    Recall: law of motion looks like:

        x' = R(x-c(x)) + y

    We have data on y but not the true distribution. We want the following:
    Given CDF/ECDF:
        ^       ,-----
        |     /
        |    /
        |   /
        +--`|--|----------->
           h0  h1
    We want P(h0 <= x' < h1)
        =  Fx(h1)-Fx(h0)
        -> Fx(h1) = Fx(x' <= h1) = Fx(R(x-c(x)) + y <= h1) = Fy(y <= h1 - R(x-c(x)))

    Let zi = hi - R(x-c(x)), ECDF = Fy from the data as an empirical CDF. Now,

        P(h0 <= x' < h1) = Fx(h1)-Fx(h0) = Fy(z1) - Fy(z0) ~= ECDF(z1) - ECDF(z0).

    We do this for all cutoffs indicated by "bins."

    Return the implied probabilities.
    '''

    cutoffs = np.array(bins)

    # Ensure that we have +/- inf book-ending the cutoffs; useful for the loop.
    if not np.isinf(cutoffs[0]):
        cutoffs = np.append(-np.inf, cutoffs)
    if not np.isinf(cutoffs[-1]):
        cutoffs = np.append(cutoffs, np.inf)

    # Set the ecdf function:
    #ecdf = ECDF(data)   # TODO: Set appropriate ECDF. Review the mquantiles
                        # function as used below

    pmf = np.ones(len(cutoffs)-1) + np.nan
    a = x0-c0
    b = R*a
    i=0
    for h0, h1 in zip(cutoffs[:-1], cutoffs[1:]):
        z0 = h0 - b
        z1 = h1 - b
        pmf[i] = cdf(z1) - cdf(z0)
        i+=1

    if np.any(pmf < 0):
        print "WARNING: np.any(pmf < 0). pmf < 0:", pmf < 0
    abs_0 = np.abs(np.sum(pmf) - 1.0)
    if not np.isclose(abs_0, 0.0):
        print "WARNING: np.sum(pmf) != 1. np.sum(pmf):", np.sum(pmf)

    return pmf




# ==============================================================================
# ============== Set up income process functions  ==============================
# ==============================================================================

def calculate_mean_one_lognormal_discrete_approx_with_cutoffs(N, sigma):
    # Note: "return" convention will be: values always come first, then probs.

    mu = -0.5*(sigma**2)
    distrib = stats.lognorm(sigma, 0, np.exp(mu))

    # ------ Set up discrete approx ------
    pdf = distrib.pdf
    invcdf = distrib.ppf

    probs_cutoffs = np.arange(N+1.0)/N      # Includes 0 and 1
    state_cutoffs = invcdf(probs_cutoffs)   # State cutoff values, each bin

    # Set pmf:
    pmf = np.repeat(1.0/N, N)

    # Find the E[X|bin] values:
    F = lambda x: x*pdf(x)
    Ebins = []

    for i, (x0, x1) in enumerate(zip(state_cutoffs[:-1], state_cutoffs[1:])):
        cond_mean1, err1 = quad(F, x0, x1, epsabs=1e-10, epsrel=1e-10, limit=200)
        # Note that the *first* to be fulfilled of epsabs and epsrel stops the
        # intregration - be aware of this when the answer is close to zero.
        # Also, if you never care about one binding (eg you'd like to force
        # scipy to use the other condition to integrate), set that = 0.
        Ebins.append(cond_mean1/pmf[i])

    X = np.array(Ebins)

    return( [X, pmf, state_cutoffs] )



def calculate_mean_one_lognormal_discrete_approx(N, sigma):
    # Note: "return" convention will be: values always come first, then probs.

    X, pmf, state_cutoffs = calculate_mean_one_lognormal_discrete_approx_with_cutoffs(N, sigma)

    return( [X, pmf] )


def find_empirical_equiprobable_bins_midpoints(N, data):
    '''
    N number of equiprobable bins and data, return the cutoffs and conditional
    expectation nodes (midpoints), and empirical probability of each bin.
    NOTE that the empirical probability will likely *not* be equal, due to the
    nature of the empirical data. As N_data -> infty, this will converge to the
    appropriate "true" equiprobable discrete value due to properties of the
    ECDF.
    '''
    # Get initial cutoffs:
    cutoffs0 = np.linspace(0,1,(N+1))
        # Need to plug into the inverse ecdf

    cutoffs = mquantiles(a=data, prob=cutoffs0, alphap=1.0/3.0, betap=1.0/3.0)
    # mquantiles(a, prob=[0.25, 0.5, 0.75], alphap=0.4, betap=0.4, axis=None, limit=())
    #  (alphap,betap) = (1/3, 1/3): p(k) = (k-1/3)/(n+1/3): Then p(k) ~ median[F(x[k])]. The resulting quantile estimates are approximately median-unbiased regardless of the distribution of x. (R type 8)

    # Set infinite upper and lower cutoffs:
    cutoffs[0] = -np.inf
    cutoffs[-1] = np.inf

    # Init containers
    EX = []
    pX = []

    for lo, hi in zip(cutoffs[:-1], cutoffs[1:]):
        bin_indx = np.logical_and(data >= lo, data < hi)
        EX.append(np.mean(data[bin_indx]))      # Should converge to correct
        pX.append(np.mean(bin_indx))            # Should also converge to proper

    EX = np.array(EX)
    pX = np.array(pX)

    return EX, cutoffs[1:-1], pX   # want to slice off the -inf and inf bin cutoffs



def find_empirical_equiprobable_bins_midpoints_old(N, data):
    '''
    Employ an inverse ECDF to determine an equiprobable N-length discretization
    of the ECDF implied by a particular set of data.

    By: Nathan M. Palmer
    Latest: 10 Feb 2015
    '''

    # If data is not 1d, make a 1d version:
    if len(np.shape(data)) > 1:
        data = data.ravel()  # Flatten the array.
        print "unraveling data..."

    data.sort()

    dataN = float(len(data))

    #weighted_data = data / dataN

    probs = np.repeat(1.0/N, N)
    #probs_cutoffs = np.arange(N+1.0)/N
    probs_cutoffs = np.cumsum(probs)*100
    probs_cutoffs = np.append(0.0, probs_cutoffs)  # manually set lower bound
    probs_cutoffs[-1] = 100.0                      # manually set upper bound


    #bin_cutoffs = np.percentile(data, probs_cutoffs) # Need to scale by 100 for np.percentiles
    intertype = 'linear'
    #bin_cutoffs = np.percentile(weighted_data, probs_cutoffs, interpolation=intertype)  # Need to scale by 100 for np.percentiles
    bin_cutoffs = np.percentile(data, probs_cutoffs, interpolation=intertype)  # Need to scale by 100 for np.percentiles
    #print "hey intertype =",intertype
    #bin_cutoffs[-1] += 1e-8  # Add "epsilon" such that we capture everything, including max point.
    bin_cutoffs[0] += -np.inf  # manaully set lower bound
    bin_cutoffs[-1] += np.inf  # manually set upper bound...

    # Now find midpoints (two ways so we can test immediatels):
    midpoints = []  #np.repeat(1.0) + np.nan  # Fill with NaNs initially.
    #midpoints2 = [] #np.repeat(1.0) + np.nan

    # Find midpoints:
    #lo = bin_cutoffs[0]
    #hi = bin_cutoffs[1]
    i = 0
    delta_weights = []
    target_1overN = 1.0 / dataN
    for lo,hi in zip(bin_cutoffs[:-1], bin_cutoffs[1:]):
        # version 1
        data_in_this_bin = np.logical_and(data >= lo, data < hi)
        temp_data = data[data_in_this_bin]
        ##data_in_this_bin = np.logical_and(weighted_data >= lo, weighted_data < hi)
        ##temp_data = weighted_data[data_in_this_bin]
        temp_n = len(temp_data)
        #midpoints.append(  fsum(temp_data)/temp_n  ) # np.mean() )
        midpoints.append(  np.mean(temp_data) )    # WHERE AT: HERE!!! THere is a correct answer here...can feel it..

        # Back out the weighting error for this bin:
        delt = (N*temp_n) / dataN
        delta_weights.append(delt)

        i += 1
        # version 2
        #midpoints1.append( lo + (hi-lo)/2.0 ) NOTE: doesn't work because non-centered distribution...

    # Array-ify:
    midpoints = np.array(midpoints) * np.array(delta_weights)
    # This adjustment will make all midpoints properly represent the equiprobably distribution.

    #midpoints2 = np.array(midpoints2)

    # QUICK CHECK TO CONFIRM that the adjustment doesn't violate the bin_cutoffs:
    i = 0
    for lo,hi in zip(bin_cutoffs[:-1], bin_cutoffs[1:]):
        assert lo <=  midpoints[i] and midpoints[i] < hi, "WARNING: assert failed for lo <=  midpoints[i] and midpoints[i] < hi; this is likely due to the re-weighting to adjust for empirical quantiles used to establish a discrete approximation to the empirical CDF. lo = " + str(lo) + ", hi = " + str(hi) + ", midpoints[i] = " + str(midpoints[i]) + ", i = " + str(i)
        i += 1

    return midpoints, bin_cutoffs[1:-1], delta_weights   # want to slice off the -inf and inf bin cutoffs



def generate_simulated_shocks_discrete(shock_points, T, N, seed=None):
    '''
    Given a set of equiprobable discrete points and integers T and N, generate
    equiprobable shocks once and simply permute them to cut down on sampling
    error in any simulation.

    See Carroll (2012) Sec 9 for an example of use of this simulation approach.

    Definitions
    -----------
    [None]

    Parameters
    ----------
    shock_points: numpy.array, 1D
        Discrete equiprobable distribution; 1D array of shock points
    T: int
        Number of periods of shocks
    N: int
        Number of agents
    seed: int or None
        Seed to set up the random number generator. Default is None, which
        will select the default numpy.random.RandomState initilization

    Returns
    -------
    y_shocks:  numpy.ndarray, (T x N)
        The matrix of shocks. Rows are periods, columns are individuals

    References
    ----------
    Carroll, C. D. (2012). Solution methods for microeconomic dynamic stochastic
    optimization problems. Manuscript, Department of Economics, Johns Hopkins
    University, 2012. http://www.econ.jhu.edu/people/ccarroll/solvingmicrodsops


    By Nathan M. Palmer
    Upated 12 March 2015
    '''
    RNG = np.random.RandomState(seed)

    # Generate the y_shocks all at once:
    Npoints = len(shock_points)
    y_shocks = []
    number_of_values_per_bin = np.floor(N / Npoints)
    unplaced_values = N % Npoints
    for i in range(Npoints):
        # First create list of values
        y_shocks += [shock_points[i]]*number_of_values_per_bin

        # Then add a new value to this if we haven't run out of unplaced_values
        if unplaced_values > 0:
            y_shocks += [shock_points[i]]
            unplaced_values -= 1 # Decrement

    # Now create list of permutations of shocks:
    y_vec = [RNG.permutation(y_shocks) for t in range(T)]
    y_shocks = np.array(y_vec)
    return y_shocks



def generate_simulated_shocks_continuous(shock_generator, T, N, seed=None):
    '''
    Function name somewhat misleading: given a random variable generator which
    produces a random numpy.ndarray object which given dimentions, produce a
    matrix of continuously rawn shocks of size (TxN).

    NOTE: this is purely a wrapper around the shock_generator, to match the
    syntax for the discrete shock generator.


    Definitions
    -----------
    [None]

    Parameters
    ----------
    shock_generator: function
        Function which can take a sequence (T,N) and return matrix
    T: int
        Number of periods of shocks
    N: int
        Number of agents

    Returns
    -------
    y_shocks:  numpy.ndarray, (T x N)
        The matrix of shocks. Rows are periods, columns are individuals

    References
    ----------
    Carroll, C. D. (2012). Solution methods for microeconomic dynamic stochastic
    optimization problems. Manuscript, Department of Economics, Johns Hopkins
    University, 2012. http://www.econ.jhu.edu/people/ccarroll/solvingmicrodsops


    By Nathan M. Palmer
    Upated 12 March 2015
    '''
    return shock_generator((T, N))



def generate_simulated_choice_data_discrete(cons, shock_points, periods, Nagents, R, discard_periods, m0, seed=None):
    '''
    The points used to approximate the policy are used to run the simulation.
    Each point in shock_points is assumed to occur with equal probability.
    Thus we can generate the income shocks once and simply permute them to cut
    down on sampling error.
    '''

    RNG = np.random.RandomState(seed)

    # Generate the y_shocks all at once:
    Npoints = len(shock_points)
    y_shocks = []
    number_of_values_per_bin = np.floor(Nagents / Npoints)
    unplaced_values = Nagents % Npoints
    for i in range(Npoints):
        # First create list of values
        y_shocks += [shock_points[i]]*number_of_values_per_bin

        # Then add a new value to this if we haven't run out of unplaced_values
        if unplaced_values > 0:
            y_shocks += [shock_points[i]]
            unplaced_values -= 1 # Decrement

    # Now create list of permutations of shocks:
    y_vec = [RNG.permutation(y_shocks) for t in range(periods)]
    y_shocks = np.array(y_vec)

    sim_m = np.zeros_like(y_shocks) + np.nan   # Set containers for fast
    sim_c = np.zeros_like(y_shocks) + np.nan   # calculation.

    if m0 is None:
        sim_m[0,] = y_shocks[0,]
    else:
        sim_m[0,] = m0

    sim_c[0,] = cons(sim_m[0,])    # Initilize consumption choice.

    # Generate m-choices:
    for t in range(0,periods-1):

        sim_m[t+1,] = R*(sim_m[t,] - sim_c[t,]) + y_shocks[t+1,]  # record next-period m
        sim_c[t+1,] = cons(sim_m[t+1,])                           # record next-period c

    # Throw out the first 20 periods:
    m = sim_m[discard_periods:,].ravel()
    return (sim_m, sim_c, y_shocks, m)


def generate_simulated_choice_data_continuous(cons, income_generator, periods, Nagents, R, discard_periods=20, m0=None):

    y_shocks = income_generator((periods, Nagents))

    sim_m = np.zeros_like(y_shocks) + np.nan   # Set containers for fast
    sim_c = np.zeros_like(y_shocks) + np.nan   # calculation.

    if m0 is None:
        sim_m[0,] = y_shocks[0,]
    else:
        sim_m[0,] = m0

    sim_c[0,] = cons(sim_m[0,])    # Initilize consumption choice.

    # Generate m-choices:
    for t in range(0,periods-1):

        sim_m[t+1,] = R*(sim_m[t,] - sim_c[t,]) + y_shocks[t+1,]  # record next-period m
        sim_c[t+1,] = cons(sim_m[t+1,])                           # record next-period c

    # Throw out the first 20 periods:
    m = sim_m[discard_periods:,].ravel()
    return (sim_m, sim_c, y_shocks, m)


def generate_simulated_choice_data_continuous_mean1_lognorm(cons, periods, Nagents, R, sigma=0.2, discard_periods=20, m0=None, seed=None):

    # Create the income generator:
    RNG = np.random.RandomState(seed)
    mu = -0.5*(sigma**2)
    income_generator = lambda n, mu=mu, sig=sigma, RNG=RNG: RNG.lognormal(mean=mu, sigma=sig, size=n)

    # Pass this into above value:
